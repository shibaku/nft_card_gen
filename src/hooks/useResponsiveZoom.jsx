import { useRef, useEffect } from "react";
// import { useMediaQuery } from "react-responsive";
import { useThree } from "@react-three/fiber";

export default function useResponsiveCamera(minWidth, maxWidth, newPos) {
  const { size, camera } = useThree();
  const initialPos = useRef(null);
  const firstRender = useRef(true);
  console.log(initialPos.current);

  useEffect(() => {
    if (firstRender.current) {
      initialPos.current = Object.values(camera.position);
      firstRender.current = false;
    }
    if (size.width > minWidth && size.width < maxWidth) {
      camera.position.set(newPos[0], newPos[1], newPos[2]);
      camera.updateProjectionMatrix();
    } else {
      const initPos = initialPos.current;
      console.log("else");
      if (!initPos) return;
      camera.position.set(initPos[0], initPos[1], initPos[2]);
      camera.updateProjectionMatrix();
    }
    // eslint-disable-next-line
  }, [size]);

  // const initialPos = useRef();
  // const min = useMediaQuery({ minWidth });
  // const max = useMediaQuery({ maxWidth });
}
