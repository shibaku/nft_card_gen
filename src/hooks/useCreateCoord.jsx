import { useEffect, useRef } from "react";
import { Spherical } from "three";

export function useCreateSphereCoord() {
  const firstRender = useRef(null);
  const sphereCoord = useRef(null);

  useEffect(() => {
    if (!firstRender) return;
    sphereCoord.current = new Spherical();
    firstRender.current = false;
  }, []);

  return sphereCoord.current;
}
