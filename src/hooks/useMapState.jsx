import shallow from "zustand/shallow";
import { useStore } from "../store/useStore";

// **** map state generator functions
function createMapState(storeName, props) {
  const entries = new Map(
    props.map((prop) => {
      return [prop, storeName((state) => state[prop])];
    })
  );
  return Object.fromEntries(entries);
}

function createMapStateArray(storeName, props) {
  return storeName((state) => props.map((prob) => state[prob]), shallow);
}

// **** map state functions
export function useMapState(...props) {
  return createMapState(useStore, props);
}

// **** map state Array functions
export function useMapStateArray(...props) {
  return createMapStateArray(useStore, props);
}
