export const shibakuVerse = {
  scene: {
    scene: {
      preloader: {
        component: "",
        backgroundImg: "/images/door.jpg",
        img: "/images/shibakulogo.png",
        // text: "<p style='text-shadow:2px 2px 6px #FFFFFF'>... welcome to the Shibaku <br/> metaverse preview ...<p>",
        text: "<p style='text-shadow:2px 2px 6px #FFFFFF' className='text-5xl'>...Metaverse...<p>",
        // autoClose: false,
      },
      afterPreloader: {
        actions: [
          {
            controller: "updateModal",
            config: {
              title: `<p className="text-3xl md:text-4xl text-center" style='text-shadow:4px 4px 4px #FFFFFF'>Welcome</p>`,
              body: `<div className="my-5 flex justify-center">
                  <div className="grid grid-cols-12 gap-y-2">
                    <div className="col-span-2 bg-white bg-opacity-30 text-center p-2 rounded-full w-10 h-10 align justify-self-end">
                      1
                    </div>
                    <div className="col-span-10 ml-3 self-center ">
                      Click and drag to look around
                    </div>
                    <div className="col-span-2 bg-white bg-opacity-30 text-center p-2 rounded-full w-10 h-10 align justify-self-end">
                      2
                    </div>
                    <div className="col-span-10 ml-3 self-center">
                      Use the menu to explore the preview
                    </div>
                    <div className="col-span-2 bg-white bg-opacity-30 text-center p-2 rounded-full w-10 h-10 align justify-self-end">
                    3
                  </div>
                  <div className="col-span-10 ml-3 self-center">
                    Press the "Music" button to enable the music
                  </div>
                  </div>
                </div>`,
            },
            state: {
              active: true,
            },
          },
        ],
      },
      camera: {
        position: {
          default: [0, 51, 188],
          mobile: [0, 81, 235],
        },
        near: 1,
        far: 10000,
        fov: 70,
        // target: [0, 42, 0],
        uiControls: false,
      },
      environment: "environment.hdr",
      render: {
        toneMappingExposure: 0.9,
        frameloop: "always",
        // frameloop: "demand",
      },
      controllers: {
        orbitControls: {
          active: true,
          enableZoom: false,
          enablePan: false,
          maxAzimuthAngle: Math.PI / 6.2,
          minAzimuthAngle: -Math.PI / 6.2,
          maxPolarAngle: Math.PI / 1.8,
          minPolarAngle: Math.PI / 2.8,
          target: {
            default: [0, 50, 50],
            mobile: [0, 65, 0],
          },
        },
      },
      gltfs: [
        {
          path: "/avatar/shibaku.glb",
          modelName: "avatar",
          scale: 100,
          position: [0, -8.5, -8],
          playAnimations: ["idle"],
        },
        {
          path: "/shibaverse/shibaverse.gltf",
          modelName: "shibakudoor",
          playAnimations: [
            "1_3.066Action",
            // "wheelAction",
            // "Cylinder_1Action",
            // "glassCover rotation",
            // "handle.001Action",
          ],
        },
      ],
      elements: {},
      modes: {},
    },
    inputs: {
      keyboard: {
        // w: {
        //   actions: [
        //     {
        //       controller: "setFpsController",
        //       config: {},
        //       state: {
        //         direction: "forward",
        //       },
        //     },
        //   ],
        // },
        // s: {
        //   actions: [
        //     {
        //       controller: "setFpsController",
        //       config: {},
        //       state: {
        //         direction: "backward",
        //       },
        //     },
        //   ],
        // },
        // a: {
        //   actions: [
        //     {
        //       controller: "setFpsController",
        //       config: {},
        //       state: {
        //         direction: "left",
        //       },
        //     },
        //   ],
        // },
        // d: {
        //   actions: [
        //     {
        //       controller: "setFpsController",
        //       config: {},
        //       state: {
        //         direction: "right",
        //       },
        //     },
        //   ],
        // },
        // " ": {
        //   actions: [
        //     {
        //       controller: "setFpsController",
        //       config: {},
        //       state: {
        //         direction: "jump",
        //       },
        //     },
        //   ],
        // },
      },
      ui: {
        ActionButtons: {
          position: "midBottom",
          name: "",
          elements: {
            spinWheel: {
              type: "button",
              name: "Spin The Wheel",
              // todo: this should be active instead of hidden
              hidden: true,
              // todo: better way for config and formatting {format: "large", "round" etc}
              large: true,
              actions: [
                {
                  controller: "setGltfAnimation",
                  config: {
                    modelName: "shibakudoor",
                    stateGroup: "coins",
                    loop: false,
                    startAt: 1.5,
                  },
                  state: {
                    animation: "wheelAction",
                  },
                },
                {
                  controller: "setGltfAnimation",
                  config: {
                    modelName: "shibakudoor",
                    stateGroup: "coins",
                    loop: false,
                  },
                  state: {
                    animation: "handle anim meAction",
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    stateGroup: "coins",
                  },
                  state: {
                    name: "lever",
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    stateGroup: "coins",
                  },
                  state: {
                    name: "wheel",
                  },
                },
              ],
            },
            shibaku: {
              type: "button",
              name: "Shibaku",
              hidden: true,
              large: true,
              actions: [
                {
                  controller: "setGltfAnimation",
                  config: {
                    modelName: "avatar",
                    stateGroup: "avatar",
                    loop: false,
                  },
                  state: {
                    animation: "jump",
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    stateGroup: "avatar",
                  },
                  state: {
                    name: "jump",
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    stateGroup: "avatar",
                  },
                  state: {
                    name: "shibaku",
                  },
                },
              ],
            },
          },
        },
        Menu: {
          position: "topLeft",
          name: "Menu",
          frame: true,
          elements: {
            welcome: {
              type: "button",
              name: "Welcome",
              icon: "",
              actions: [
                {
                  // ! change to setCameraPosition
                  controller: "setCameraTween",
                  // ! is this the best and scalable way to do this?
                  // how is this passed to setActionController?
                  config: {
                    //! add a loop prop
                    duration: 1350,
                    tween: true,
                    //! maybe good to add a type for each action?
                  },
                  state: {
                    position: {
                      default: [0, 51, 188],
                      mobile: [0, 81, 235],
                    },
                  },
                },
                {
                  // !change to setCameraTarget
                  controller: "setCameraTargetTween",
                  config: {
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    target: {
                      default: [0, 50, 0],
                      mobile: [0, 65, 0],
                    },
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    ignoreStateGroup: true,
                  },
                  state: {
                    name: "camera",
                  },
                },
              ],
            },
            haikuView: {
              type: "button",
              name: "Haikus",
              icon: "",
              actions: [
                {
                  controller: "setCameraTween",
                  config: {
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    // position: [0, 55, 80],
                    position: {
                      default: [0, 55, 80],
                      mobile: [0, 55, 150],
                    },
                  },
                  onComplete: [
                    {
                      controller: "updateModal",
                      config: {
                        useOnce: true,
                        view: "shibaku-verse",
                      },
                      state: {
                        active: true,
                        title: `<p className="text-3xl md:text-4xl text-center" style='text-shadow:4px 4px 4px #FFFFFF'>Words Of Wisdom</p>`,
                        body: `
                              <div className="my-5 flex justify-center leading-normal md:leading-relaxed">
                                <div className="text-base md:text-xl text-center leading-relaxed">
                                  Coming soon, listen <br/> to the "Dog Whisperer" soothing <br/>
                                  your mind with Haikus of wisdom!
                                </div>
                              </div>
                            `,
                      },
                    },
                  ],
                },
                {
                  controller: "setCameraTargetTween",
                  config: {
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    target: {
                      default: [0, 55, 0],
                      mobile: [0, 55, 0],
                    },
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    ignoreStateGroup: true,
                  },
                  state: {
                    name: "camera",
                  },
                },
              ],
            },
            coinsView: {
              type: "button",
              name: "Coin Flipper",
              icon: "",
              actions: [
                {
                  controller: "setCameraTween",
                  config: {
                    stateGroup: "coins",
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    position: {
                      default: [0, 16, 145],
                      mobile: [0, 30, 240],
                    },
                  },
                  onComplete: [
                    {
                      controller: "updateModal",
                      config: {
                        useOnce: true,
                        stateGroup: "coins",
                        view: "shibaku-verse",
                      },
                      state: {
                        active: true,
                        title: `<p className="text-3xl md:text-4xl text-center"  style='text-shadow:4px 4px 4px #FFFFFF'>Coins Of Destiny</p>`,
                        body: `
                              <div className="my-5 flex justify-center leading-normal md:leading-relaxed">
                                <div className="text-base md:text-xl text-center">
                                  Coming Q1 2022, <br/> play the "Coins Of Destiny"<br/>
                                  to win awesome prizes  and rewards!<br/>
                                  Click the "Spin The Wheel" button to try!
                                </div>
                              </div>
                              `,
                      },
                    },
                    {
                      controller: "updateViewsStoreTarget",
                      config: {
                        stateGroup: "coins",
                        name: "toggleSpinButton",
                        type: "toggle",
                        // ! add this programtically
                        view: "shibaku-verse",
                        //! is there a better way to do this?
                        // remove hidden and add into state:{hidden:false}
                        targetPath:
                          "inputs.ui.ActionButtons.elements.spinWheel",
                      },
                      state: {
                        hidden: false,
                      },
                      // ! read initial/previous state from store by default
                      // ! if defaultState is set read from default state
                      // with this default state can be updated based on interactions
                      // but no default state has to be set for simple group on off behavior
                      defaultState: {
                        hidden: true,
                      },
                    },
                  ],
                },
                {
                  controller: "setCameraTargetTween",
                  config: {
                    stateGroup: "coins",
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    target: {
                      default: [0, 16, 0],
                      mobile: [0, 50, 0],
                    },
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    ignoreStateGroup: true,
                  },
                  state: {
                    name: "camera",
                  },
                },
              ],
            },
            avatarView: {
              type: "button",
              name: "Avatar",
              icon: "",
              actions: [
                {
                  controller: "setCameraTween",
                  config: {
                    stateGroup: "avatar",
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    position: {
                      default: [0, 6, 21],
                      mobile: [0, 6, 45],
                    },
                  },
                  onComplete: [
                    {
                      controller: "updateModal",
                      config: {
                        useOnce: true,
                        stateGroup: "avatar",
                        // stateGroup: "coins",
                        view: "shibaku-verse",
                      },
                      state: {
                        active: true,
                        title: `<p className="text-3xl md:text-4xl text-center" style='text-shadow:4px 4px 4px #FFFFFF'>Find The Elders</p>`,
                        body: `
                              <div className="my-5 flex justify-center ">
                                <div className="text-base md:text-xl text-center leading-relaxed">
                                  Coming 2022, <br/> play our RPG Game 
                                  and help <br/> the Shibakus to find The Elders!
                                </div>
                              </div>
                            `,
                      },
                    },
                    {
                      controller: "updateViewsStoreTarget",
                      config: {
                        stateGroup: "avatar",
                        name: "toggleSpinButton",
                        type: "toggle",
                        // ! add this programtically
                        view: "shibaku-verse",
                        //! is there a better way to do this?
                        // remove hidden and add into state:{hidden:false}
                        targetPath: "inputs.ui.ActionButtons.elements.shibaku",
                      },
                      state: {
                        hidden: false,
                      },
                      // ! read initial/previous state from store in future
                      defaultState: {
                        hidden: true,
                      },
                    },
                  ],
                },
                {
                  controller: "setCameraTargetTween",
                  config: {
                    stateGroup: "avatar",
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    target: {
                      default: [0, 5.5, 0],
                      mobile: [0, 5, 0],
                    },
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    ignoreStateGroup: true,
                  },
                  state: {
                    name: "camera",
                  },
                },
              ],
            },
            music: {
              type: "button",
              name: "Music",
              icon: "",
              actions: [
                {
                  controller: "setAudio",
                  config: {
                    ignoreStateGroup: true,
                  },
                  state: {
                    name: "backgroundDefault",
                  },
                },
              ],
            },
          },
        },
      },
    },
    audio: {
      backgroundDefault: {
        state: {},
        config: {
          url: "/audio/backgroundDefault.mp3",
          loop: true,
          volume: 0.2,
        },
      },
      jump: {
        state: {},
        config: {
          url: "/audio/jump.mp3",
        },
      },
      shibaku: {
        state: {},
        config: {
          url: "/audio/shibaku.mp3",
          useOnce: true,
        },
      },
      lever: {
        state: {},
        config: {
          url: "/audio/lever.mp3",
        },
      },
      camera: {
        state: {},
        config: {
          url: "/audio/camera.mp3",
          interrupt: true,
          volume: 0.7,
        },
      },
      wheel: {
        state: {},
        config: {
          url: "/audio/wheel.mp3",
          playTillEnd: true,
          volume: 0.2,
        },
      },
    },
  },
};

// keyboard: {
//   1: {
//     actions: [
//       {
//         controller: "setCameraTween",
//         config: {
//           duration: 2000,
//           tween: true,
//         },
//         state: {
//           position: [0, 16, 145],
//         },
//       },
//       {
//         controller: "setCameraTargetTween",
//         config: {
//           duration: 2000,
//           tween: true,
//         },
//         state: {
//           target: [0, 16, 0],
//         },
//       },
//     ],
//   },
//   2: {
//     actions: [
//       {
//         controller: "setGltfAnimation",
//         config: {
//           modelName: "shibakudoor",
//           loop: false,
//         },
//         state: {
//           animation: "handle anim meAction",
//         },
//       },
//     ],
//   },
//   3: {
//     actions: [
//       {
//         controller: "setGltfAnimation",
//         config: {
//           modelName: "shibakudoor",
//           loop: false,
//         },
//         state: {
//           animation: "wheelAction",
//         },
//       },
//     ],
//   },
//   4: {
//     actions: [
//       {
//         controller: "setCameraTween",
//         config: {
//           duration: 3000,
//           tween: true,
//         },
//         state: {
//           position: [0, 6, 21],
//         },
//       },
//       {
//         controller: "setCameraTargetTween",
//         config: {
//           duration: 3000,
//           tween: true,
//         },
//         state: {
//           target: [0, 5.5, 0],
//         },
//       },
//     ],
//   },
//   5: {
//     actions: [
//       {
//         controller: "setGltfAnimation",
//         config: {
//           modelName: "avatar",
//           loop: false,
//           // effectiveTimeScale: 0.5,
//         },
//         state: {
//           animation: "jump",
//         },
//       },
//     ],
//   },
// },
