export const gallery = {
  scene: {
    scene: {
      preloader: {
        component: "",
        backgroundImg: "/images/gallery.jpg",
        img: "/images/tribesdao.png",
        // text: "<p style='text-shadow:2px 2px 6px #FFFFFF'>... welcome to the Shibaku <br/> metaverse preview ...<p>",
        text: "<p style='text-shadow:2px 2px 6px #FFFFFF' className='text-5xl'>...Head Quaters...<p>",
        // autoClose: false,
      },
      afterPreloader: {
        actions: [
          {
            controller: "updateModal",
            config: {},
            state: {
              active: true,
              title: `<p className="text-3xl md:text-4xl text-center" style='text-shadow:4px 4px 4px #FFFFFF'>Welcome</p>`,
              body: `<div className="my-5 flex justify-center">
                      <div className="grid grid-cols-12 gap-y-2">
                        <div className="col-span-2 bg-white bg-opacity-30 text-center p-2 rounded-full w-10 h-10 align justify-self-end">
                          1
                        </div>
                        <div className="col-span-10 ml-3 self-center ">
                          Click and drag to look around
                        </div>
                        <div className="col-span-2 bg-white bg-opacity-30 text-center p-2 rounded-full w-10 h-10 align justify-self-end">
                          2
                        </div>
                        <div className="col-span-10 ml-3 self-center ">
                          Use the menu to enter the gallery
                        </div>
                      </div>
                    </div>`,
            },
          },
        ],
      },
      camera: {
        position: {
          // default: [0, 20, 20],
          default: [0, 0, 190],
          mobile: [0, 0, 380],
        },
        near: 1,
        far: 1000,
        fov: 70,
        // target: [0, 42, 0],
        uiControls: false,
      },
      environment: "environment.hdr",
      render: {
        toneMappingExposure: 0.9,
        frameloop: "always",
        // frameloop: "demand",
      },
      controllers: {
        // todo:consistent naming, need to update all components and state refs
        orbitControls: {
          active: true,
          makeDefault: true,
          // enableZoom: false,
          // enablePan: false,
          // maxAzimuthAngle: Math.PI / 6.2,
          // minAzimuthAngle: -Math.PI / 6.2,
          // maxPolarAngle: Math.PI / 1.8,
          // minPolarAngle: Math.PI / 2.8,
          target: {
            default: [0, 0, 0],
            mobile: [0, 0, 0],
          },
        },
        fpsControls: {
          active: false,
        },
        transformController: {
          active: true,
          config: {
            logUpdates: true,
          },
        },
      },
      gltfs: [
        {
          path: "/gallery/bridge.gltf",
          modelName: "gallery",
          // scale: 1,
          // position: [0, , -8],
          // todo: add bool option playAnimations:true plays all animation, array only selected anim
          playAnimations: [
            "screen anim meAction",
            "lightsrips.001Action.001",
            "lightsrips.001Action.002",
            "lightsrips.001Action.003",
            "lightsrips.001Action.004",
            "lightsrips.001Action.005",
            "lightsrips.001Action.006",
            "lightsrips.002Action.002",
          ],
          enableTransforms: false,
        },
        // {
        //   path: "/avatar/shibaku.glb",
        //   modelName: "shibaku",
        //   position: [0, 3, 3],
        //   scale: 30,
        //   enableTransforms: true,
        // },
      ],
      elements: {
        skyDome: {
          type: "skyDome",
          config: {
            radius: 400,
            widthSegments: 32,
            heightSegments: 16,
            emission: 1,
            texture: "/sky.jpg",
            lightMap: "/lightmap.jpg",
            // controls: true,
          },
        },
        screen_01: {
          type: "plane",
          config: {
            position: [-74.22, 0.59, 8.69],
            rotation: [0, 1.56, 0],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              // stream: "dash", //not used right now, default stream always dash
              src: {
                thumbnail: "/thumbnails/screenshot-000 13.jpg",
                mp4: "/videos/13.mp4",
                dash: "https://videodelivery.net/e77730d30974f44f015c8c8b0e4f8733/manifest/video.mpd",
                hls: "https://videodelivery.net/e77730d30974f44f015c8c8b0e4f8733/manifest/video.m3u8",
              },
            },
          },
        },
        screen_02: {
          type: "plane",
          config: {
            position: [-73.66, 0.62, 0.13],
            rotation: [0, 1.44, 0],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 12.jpg",
                mp4: "/videos/12.mp4",
                hls: "https://videodelivery.net/cfccf507c9a2cd4e8ce687b7520a6501/manifest/video.m3u8",
                dash: "https://videodelivery.net/cfccf507c9a2cd4e8ce687b7520a6501/manifest/video.mpd",
              },
            },
          },
        },
        screen_03: {
          type: "plane",
          config: {
            position: [-55, 5.12, -21.08],
            rotation: [0, 0.278, 0],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 2.jpg",
                mp4: "/videos/2.mp4",
                hls: "https://videodelivery.net/69d8441a721abf87b66332ce07025242/manifest/video.m3u8",
                dash: "https://videodelivery.net/69d8441a721abf87b66332ce07025242/manifest/video.mpd",
              },
            },
          },
        },
        screen_04: {
          type: "plane",
          config: {
            position: [-27.16, 5.12, -22.43],
            rotation: [0, 0.025, 0],
            width: 4,
            height: 4,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 18.jpg",
                mp4: "/videos/18.mp4",
                hls: "https://videodelivery.net/b7d71bb5d33fdaf04b8b92dae54a32b8/manifest/video.m3u8",
                dash: "https://videodelivery.net/b7d71bb5d33fdaf04b8b92dae54a32b8/manifest/video.mpd",
              },
            },
          },
        },
        screen_05: {
          type: "plane",
          config: {
            position: [0.86, 11.52, -59.72],
            rotation: [0, 0, 0],
            width: 14,
            height: 14,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 17.jpg",
                mp4: "/videos/17.mp4",
                hls: "https://videodelivery.net/8cd9edef944e0069732e47dd3f576ea7/manifest/video.m3u8",
                dash: "https://videodelivery.net/8cd9edef944e0069732e47dd3f576ea7/manifest/video.mpd",
              },
            },
          },
        },
        screen_06: {
          type: "plane",
          config: {
            position: [27.9, 5.12, -22.43],
            rotation: [0, 0.025, 0],
            width: 4,
            height: 4,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 8.jpg",
                mp4: "/videos/8.mp4",
                hls: "https://videodelivery.net/41cffe43b4cd793a08b9595e17498de1/manifest/video.m3u8",
                dash: "https://videodelivery.net/41cffe43b4cd793a08b9595e17498de1/manifest/video.mpd",
              },
            },
          },
        },
        screen_07: {
          type: "plane",
          config: {
            position: [55.9, 5.12, -22.43],
            rotation: [0, -0.3, 0],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 3.jpg",
                mp4: "/videos/3.mp4",
                hls: "https://videodelivery.net/b141ca09fa1cddb48e7d0b85e669950f/manifest/video.m3u8",
                dash: "https://videodelivery.net/b141ca09fa1cddb48e7d0b85e669950f/manifest/video.mpd",
              },
            },
          },
        },
        screen_08: {
          type: "plane",
          config: {
            position: [77.04, 0.28, -2.16],
            rotation: [0, -1.366, 0],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 10.jpg",
                mp4: "/videos/10.mp4",
                hls: "https://videodelivery.net/3887ba6c58b6c85abd92429e27a5eae2/manifest/video.m3u8",
                dash: "https://videodelivery.net/3887ba6c58b6c85abd92429e27a5eae2/manifest/video.mpd",
              },
            },
          },
        },
        screen_09: {
          type: "plane",
          config: {
            position: [78.26, 0.32, 7.13],
            rotation: [0, -1.519, 0],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 14.jpg",
                mp4: "/videos/14.mp4",
                hls: "https://videodelivery.net/d8231776dc6cbb3a5a3f5232997a1c94/manifest/video.m3u8",
                dash: "https://videodelivery.net/d8231776dc6cbb3a5a3f5232997a1c94/manifest/video.mpd",
              },
            },
          },
        },
        screen_10: {
          type: "plane",
          config: {
            position: [50.09, 8.234, 11.94],
            rotation: [3.14, -0.3, 3.14],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 19.jpg",
                mp4: "/videos/19.mp4",
                hls: "https://videodelivery.net/a73c3bd2b83d737af14caac9c2ad6665/manifest/video.m3u8",
                dash: "https://videodelivery.net/a73c3bd2b83d737af14caac9c2ad6665/manifest/video.mpd",
              },
            },
          },
        },
        screen_11: {
          type: "plane",
          config: {
            position: [17.277, 7.944, 25.07],
            rotation: [-3.14, -0.098, -3.14],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 5.jpg",
                mp4: "/videos/5.mp4",
                hls: "https://videodelivery.net/87c2a58472e90d80f747ef48bc13b662/manifest/video.m3u8",
                dash: "https://videodelivery.net/87c2a58472e90d80f747ef48bc13b662/manifest/video.mpd",
              },
            },
          },
        },
        screen_12: {
          type: "plane",
          config: {
            position: [-20.25, 7.944, 25.17],
            rotation: [0, 0.35, 0],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 11.jpg",
                mp4: "/videos/11.mp4",
                hls: "https://videodelivery.net/b8031cda6bfd171d5ab35515a2353d4b/manifest/video.m3u8",
                dash: "https://videodelivery.net/b8031cda6bfd171d5ab35515a2353d4b/manifest/video.mpd",
              },
            },
          },
        },
        screen_13: {
          type: "plane",
          config: {
            position: [-52.52, 8.299, 11.79],
            rotation: [-3.14, 0.26, -3.14],
            width: 6,
            height: 6,
            material: {
              type: "video",
              side: "DoubleSide",
              src: {
                thumbnail: "/thumbnails/screenshot-000 6.jpg",
                mp4: "/videos/6.mp4",
                hls: "https://videodelivery.net/d9b6eee7dbbb6da5354626ad76f9d28e/manifest/video.m3u8",
                dash: "https://videodelivery.net/d9b6eee7dbbb6da5354626ad76f9d28e/manifest/video.mpd",
              },
            },
          },
        },
      },
    },
    inputs: {
      joysticks: {
        joystickRight: {
          active: false,
          config: {
            position: "right",
            // todo:add responsive disable config, only use on mobile
          },
          forward: {
            actions: [
              {
                controller: "setFpsController",
                config: {},
                //todo: active:true state should come from here not from within useInputControllers
                state: {
                  direction: "rotateUp",
                },
              },
            ],
          },
          backward: {
            actions: [
              {
                controller: "setFpsController",
                config: {},
                state: {
                  direction: "rotateDown",
                },
              },
            ],
          },
          left: {
            actions: [
              {
                controller: "setFpsController",
                config: {},
                state: {
                  direction: "rotateLeft",
                },
              },
            ],
          },
          right: {
            actions: [
              {
                controller: "setFpsController",
                config: {},
                state: {
                  direction: "rotateRight",
                },
              },
            ],
          },
        },
        joystickLeft: {
          active: false,
          config: {
            position: "left",
          },
          forward: {
            actions: [
              {
                controller: "setFpsController",
                config: {},
                state: {
                  direction: "forward",
                },
              },
            ],
          },
          backward: {
            actions: [
              {
                controller: "setFpsController",
                config: {},
                state: {
                  direction: "backward",
                },
              },
            ],
          },
          left: {
            actions: [
              {
                controller: "setFpsController",
                config: {},
                state: {
                  direction: "left",
                },
              },
            ],
          },
          right: {
            actions: [
              {
                controller: "setFpsController",
                config: {},
                state: {
                  direction: "right",
                },
              },
            ],
          },
        },
      },
      keyboard: {
        w: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "forward",
              },
            },
          ],
        },
        ArrowUp: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "forward",
              },
            },
          ],
        },
        s: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "backward",
              },
            },
          ],
        },
        ArrowDown: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "backward",
              },
            },
          ],
        },
        a: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "left",
              },
            },
          ],
        },
        ArrowLeft: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "left",
              },
            },
          ],
        },
        d: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "right",
              },
            },
          ],
        },
        ArrowRight: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "right",
              },
            },
          ],
        },
        " ": {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "jump",
              },
            },
            // {
            //   controller: "setAudio",
            //   config: {
            //     ignoreStateGroup: true,
            //     type: "keydown",
            //   },
            //   state: {
            //     name: "jump",
            //   },
            // },
          ],
        },
        0: {
          actions: [
            {
              controller: "setFpsController",
              config: {},
              state: {
                direction: "jump",
              },
            },
          ],
        },
        r: {
          actions: [
            {
              controller: "setTransFormController",
              config: {
                type: "keydown",
              },
              state: {
                mode: "rotate",
              },
            },
          ],
        },
        t: {
          actions: [
            {
              controller: "setTransFormController",
              config: {
                type: "keydown",
              },
              state: {
                mode: "translate",
              },
            },
          ],
        },
        y: {
          actions: [
            {
              controller: "setTransFormController",
              config: {
                type: "keydown",
              },
              state: {
                mode: "scale",
              },
            },
          ],
        },
      },
      // todo: ui shouldn't be nested under inputs
      ui: {
        Menu: {
          position: "topLeft",
          name: "Menu",
          frame: true,
          groupName: true,
          elements: {
            gallery: {
              type: "button",
              name: "Gallery",
              icon: "",
              actions: [
                {
                  controller: "setCameraTween",
                  config: {
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    position: {
                      default: [0, 0, 35],
                      mobile: [0, 0, 35],
                    },
                  },
                  onComplete: [
                    {
                      controller: "updateViewsStoreTarget",
                      config: {
                        type: "toggle",
                        view: "gallery",
                        targetPath: "scene.controllers.orbitControls",
                      },
                      state: {
                        active: false,
                      },
                    },
                    {
                      controller: "updateViewsStoreTarget",
                      config: {
                        type: "toggle",
                        view: "gallery",
                        targetPath: "scene.controllers.fpsControls",
                      },
                      state: {
                        active: true,
                      },
                    },
                    {
                      controller: "updateViewsStoreTarget",
                      config: {
                        type: "toggle",
                        view: "gallery",
                        targetPath: "inputs.joysticks.joystickRight",
                      },
                      state: {
                        active: true,
                      },
                    },
                    {
                      controller: "updateViewsStoreTarget",
                      config: {
                        type: "toggle",
                        view: "gallery",
                        targetPath: "inputs.joysticks.joystickLeft",
                      },
                      state: {
                        active: true,
                      },
                    },
                    {
                      controller: "updateViewsStoreTarget",
                      config: {
                        type: "toggle",
                        view: "gallery",
                        targetPath: "inputs.ui.MobileJump.elements.jump",
                      },
                      state: {
                        hidden: false,
                      },
                    },
                    {
                      controller: "updateModal",
                      config: {
                        useOnce: true,
                        view: "gallery",
                      },
                      state: {
                        active: true,
                        title: `<p className="text-3xl md:text-4xl text-center" style='text-shadow:4px 4px 4px #FFFFFF'>Gallery</p>`,
                        body: ` 
                        <div className="my-3 justify-center leading-normal md:leading-relaxed hidden xl:flex">
                          <div className="text-base md:text-xl text-center leading-relaxed">
                            Use the <br/> 
                          <span className='border-2 rounded p-1 w-8 mx-auto mb-2 mt-2 block'>w</span>
                          <span className='border-2 rounded p-1'>a</span>
                          <span className='border-2 rounded p-1'>s</span>
                          <span className='border-2 rounded p-1 mb-2 inline-block'>d</span>
                          <br/>
                          Keys to move around 
                          <br/>
                          And jump by hitting
                          <span className='border-2 rounded p-1 block mt-2 mb-2'>space</span>
                        </div>
                      </div>
                      <div className="my-3 flex justify-center leading-normal md:leading-relaxed xl:hidden">
                          <div className="text-base md:text-xl text-center leading-relaxed">
                            Use the they joysticks below<br/>
                            To move and look around<br/>
                            Jump by hitting the middle button 
                        </div>
                      </div>
                      `,
                      },
                    },
                  ],
                },
                {
                  controller: "setCameraTargetTween",
                  config: {
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    target: {
                      default: [0, 5, 0],
                      mobile: [0, 5, 0],
                    },
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    ignoreStateGroup: true,
                  },
                  state: {
                    name: "camera",
                  },
                },
              ],
            },
            spaceShip: {
              type: "button",
              name: "Space Ship",
              icon: "",
              actions: [
                {
                  controller: "updateViewsStoreTarget",
                  config: {
                    type: "toggle",
                    view: "gallery",
                    targetPath: "inputs.ui.MobileJump.elements.jump",
                  },
                  state: {
                    hidden: true,
                  },
                },
                {
                  controller: "updateViewsStoreTarget",
                  config: {
                    // name: "toggleSpinButton",
                    type: "toggle",
                    view: "gallery",
                    targetPath: "scene.controllers.orbitControls",
                  },
                  state: {
                    active: true,
                  },
                },
                {
                  controller: "updateViewsStoreTarget",
                  config: {
                    // stateGroup: "coins",
                    // name: "toggleSpinButton",
                    type: "toggle",
                    view: "gallery",
                    targetPath: "scene.controllers.fpsControls",
                  },
                  state: {
                    active: false,
                  },
                },
                {
                  controller: "updateViewsStoreTarget",
                  config: {
                    // name: "toggleSpinButton",
                    type: "toggle",
                    view: "gallery",
                    targetPath: "inputs.joysticks.joystickRight",
                  },
                  state: {
                    active: false,
                  },
                },
                {
                  controller: "updateViewsStoreTarget",
                  config: {
                    // name: "toggleSpinButton",
                    type: "toggle",
                    view: "gallery",
                    targetPath: "inputs.joysticks.joystickLeft",
                  },
                  state: {
                    active: false,
                  },
                },
                {
                  controller: "setCameraTween",
                  config: {
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    // position: [0, 55, 80],
                    position: {
                      default: [0, 0, 190],
                      mobile: [0, 0, 380],
                    },
                  },
                },
                {
                  controller: "setCameraTargetTween",
                  config: {
                    duration: 1350,
                    tween: true,
                  },
                  state: {
                    target: {
                      default: [0, 0, 0],
                      mobile: [0, 0, 0],
                    },
                  },
                },
                {
                  controller: "setAudio",
                  config: {
                    ignoreStateGroup: true,
                  },
                  state: {
                    name: "camera",
                  },
                },
              ],
            },
            music: {
              type: "button",
              name: "Music",
              icon: "",
              actions: [
                {
                  controller: "setAudio",
                  config: {
                    ignoreStateGroup: true,
                  },
                  state: {
                    name: "backgroundDefault",
                  },
                },
              ],
            },
          },
        },
        // todo: unfied data structure?, {coinfg, active, elements,}
        MobileJump: {
          position: "bottomCenter",
          name: "Menu",
          frame: false,
          groupName: false,
          elements: {
            jump: {
              type: "button",
              name: null,
              round: true,
              hidden: true,
              mobileOnly: true,
              icon: "/images/jump.png",
              // todo: how to extend this?
              //able to select if active on bottom up or down
              // selectively change click and mouseUp state for each action
              // maybe best: onClickActions: [], onMouseUpActions: [], onMouseDownActions: []
              onMouseUp: "revert",
              actions: [
                {
                  controller: "setFpsController",
                  config: {},
                  state: {
                    direction: "jump",
                    active: true,
                  },
                },
              ],
            },
          },
        },
      },
    },
    audio: {
      backgroundDefault: {
        state: {},
        config: {
          url: "/audio/backgroundMystic.mp3",
          loop: true,
          volume: 0.3,
        },
      },
      // jump: {
      //   state: {},
      //   config: {
      //     url: "/audio/jump-2.mp3",
      //     volume: 0.3,
      //   },
      // },
      camera: {
        state: {},
        config: {
          url: "/audio/camera-2.mp3",
          interrupt: true,
          volume: 0.7,
        },
      },
      // walk: {
      //   state: {},
      //   config: {
      //     url: "/audio/foodSteps.mp3",
      //     loop: true,
      //     interrupt: true,
      //     playbackRate: 1.3,
      //     volume: 0.3,
      //   },
      // },
    },
  },
};
