// import EditorView from "./components/EditorView";
// import RenderView from "./components/RenderView";
// import RenderSettingsView from "./components/RenderSettingsView";
// import Haiku from "./components/Haiku";
// import Shibaku from "./components/Shibaku";
// import Bridge from "./components/Bridge";
import Home from "./components/home/Home";
import { useAppStore } from "./store/useAppStore";
import Main from "./components/vxr";
// import TestStream from "components/TestStream";
// import TestStream3D from "components/TestStream3D";

export const routes = {
  default: [
    // { path: "/render", component: RenderView },
    // { path: "/renderSettings", component: RenderSettingsView },
    { path: "/", component: Home, exact: true },
    // { path: "/test", component: TestStream3D },
    // {
    //   path: "/gallery",
    //   component: Main,
    //   name: "Gallery",
    // },
  ],

  menu: [
    // {
    //   path: "/editorView",
    //   component: EditorView,
    //   imgPath: "/images/editor.jpg",
    //   name: "Editor",
    // },
    // {
    //   path: "/haiku",
    //   component: Haiku,
    //   imgPath: "/images/shiba-haiku.jpg",
    //   name: "Haiku",
    // },
    // {
    //   path: "/shibaku",
    //   component: Shibaku,
    //   imgPath: "/images/shibaku.jpg",
    //   name: "Avatar",
    // },
    {
      path: "/shibaku-verse",
      component: Main,
      imgPath: "/images/door.jpg",
      name: "Shibaku Metaverse",
    },
    {
      path: "/gallery",
      component: Main,
      imgPath: "/images/gallery.jpg",
      name: "Gallery",
    },
  ],
};

const menuItems = routes.menu.map(({ path, imgPath, name }) => ({
  path,
  imgPath,
  name,
}));

useAppStore.setState({ menuItems });
export const allRoutes = [...routes.default, ...routes.menu];
