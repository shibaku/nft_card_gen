import { useEffect } from "react";
import { useThree } from "@react-three/fiber";

export default function useUpdateCamPosition(x, y, z) {
  const { camera } = useThree();
  useEffect(() => {
    camera.position.set(x, y, z);
    camera.updateProjectionMatrix();
  });
}
