import { useThree } from "@react-three/fiber";
import { useMemo } from "react";

export default function useToneMapping(value) {
  const { gl } = useThree();
  useMemo(() => (gl.toneMappingExposure = value), [gl, value]);
}
