import { BackSide, TextureLoader } from "three";
import { useLoader } from "@react-three/fiber";
import { skyDome } from "../data/renderSettigs.json";

export default function SkyDomeRender() {
  const background = useLoader(TextureLoader, "./sky.jpg");
  const lightMap = useLoader(TextureLoader, "./lightmap.jpg");

  // !todo: change side from double to inside

  return (
    <mesh visible={skyDome.active}>
      <sphereGeometry
        args={[skyDome.radius, skyDome.widthSeg, skyDome.heightSeg]}
      />
      <meshBasicMaterial
        side={BackSide}
        map={background}
        lightMap={lightMap}
        lightMapIntensity={skyDome.emission}
      />
    </mesh>
  );
}
