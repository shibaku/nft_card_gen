import { Canvas } from "@react-three/fiber";
import HaikuScene from "./haiku/HaikuScene";
import NewHaikuButton from "./haiku/NewHaikuButton";

export default function Haiku() {
  return (
    <div className="h-screen w-screen bg-black ">
      <Canvas
        camera={{
          position: [0, 0.016, 0.1],
          near: 0.01,
          far: 2000,
          fov: 70,
        }}
        frameloop="demand"
      >
        <HaikuScene />
      </Canvas>
      <NewHaikuButton />
    </div>
  );
}
