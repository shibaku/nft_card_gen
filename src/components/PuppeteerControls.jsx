import { useMemo } from "react";
import { useMapState } from "hooks/useMapState";
import mapCSV from "data/mapMeta.csv";
import mapHaikuCsv from "data/mapHaiku.csv";
import mapException from "data/mapMetaExceptions.csv";
import mapMetaToStore from "data/mapMetaToStore.csv";
// import { useState } from "react";
// import { sampleMeta } from "data/sampleMetadata";

import {
  mapCsvToObj,
  exceptionCsvToObj,
  metaDataCsvToObj,
  getHaikuNodes,
  tokenIdDigits,
  mapMetaToStoreCsvToObj,
} from "utils/metadataUtils";

export default function PuppeteerControls() {
  const mapMeta = useMemo(() => mapCsvToObj(mapCSV), []);
  const exceptions = useMemo(() => exceptionCsvToObj(mapException), []);
  const mapMetaStore = useMemo(
    () => mapMetaToStoreCsvToObj(mapMetaToStore),
    []
  );
  // const [sampleIndex, setSampleIndex] = useState(0); //!testing
  const state = useMapState(
    "modelLoaded",
    "increaseCameraPhi",
    "resetCameraPhi",
    "waitForRender",
    "setMetadata"
  );

  // read metadata from dom element created by puppeteer
  const handleClick = () => {
    console.log("on submit event");
    const metadata = document.tokenMetadata;

    // const newIndex = sampleIndex + 1; // !testing
    // setSampleIndex(newIndex); //!
    // const metadata = sampleMeta[newIndex]; // !testing

    // todo: rename function metaDataToObj
    const metaAttr = metaDataCsvToObj(metadata, mapMetaStore);
    console.log("added metadata");
    const haikuNodes = getHaikuNodes(mapHaikuCsv, metaAttr.haiku);

    const tokenIdArr = tokenIdDigits(metaAttr.token_id);
    const isGolden = metaAttr.golden === "false" ? false : true;
    state.setMetadata({
      metaData: metaAttr,
      mapMeta,
      exceptions,
      haikuNodes,
      tokenIdArr,
      isGolden,
    });
    state.resetCameraPhi();
  };

  return (
    <>
      {state.modelLoaded && (
        <div className="hidden" id="model-loaded">
          Model Loaded
        </div>
      )}
      <button
        onClick={state.increaseCameraPhi}
        // when button hidden, error in puppeteer
        // className="absolute top-0" //!testing
        className="absolute top-0 opacity-0" //!testing
        id="next-frame"
      >
        Render Next Frame
      </button>
      {!state.waitForRender && (
        <div className="hidden" id="render-ready">
          Next Frame Ready
        </div>
      )}
      <button
        onClick={handleClick}
        type="submit"
        id="update-metadata"
        className="absolute top-20 opacity-0"
        // className="absolute top-20 bg-blue-300" //!testing
      >
        Update Metadata
      </button>
    </>
  );
}
