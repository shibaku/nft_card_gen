// import { EffectComposer, Bloom } from "@react-three/postprocessing";
import { Vector2 } from "three";
import { useEffect, useRef, useMemo } from "react";
import { extend, useThree, useFrame } from "@react-three/fiber";
import { useControls } from "leva";

import { EffectComposer } from "three-stdlib/postprocessing/EffectComposer";
import { RenderPass } from "three-stdlib/postprocessing/RenderPass";
import { UnrealBloomPass } from "three-stdlib/postprocessing/UnrealBloomPass";
import { ShaderPass } from "three-stdlib/postprocessing/ShaderPass";
// import { BokehPass } from "three-stdlib/postprocessing/BokehPass";
import { SAOPass } from "three-stdlib/postprocessing/SAOPass";

import { FXAAShader } from "three-stdlib/shaders/FXAAShader";
import { VignetteShader } from "shaders/VignetteShader";
import { HueSaturationShader } from "shaders/hueSaturationShader";

extend({
  EffectComposer,
  RenderPass,
  UnrealBloomPass,
  ShaderPass,
  SAOPass,
  // BokehPass,
});

export default function PostprocessingCustom() {
  const composer = useRef();
  const { scene, gl, size, camera } = useThree();
  const { hue, saturation } = useControls({
    hue: {
      value: 0,
      min: -1,
      max: 1,
      step: 0.01,
    },
    saturation: {
      value: -0.1,
      min: -1,
      max: 1,
      step: 0.01,
    },
  });
  const { bloom, treshold, radius } = useControls({
    bloom: 0.15,
    radius: 0.12,
    treshold: 0.08,
  });
  // const { focus, aperture, maxblur } = useControls({
  //   focus: 0.1,
  //   aperture: {
  //     value: 0.003,
  //     step: 0.001,
  //     min: 0.001,
  //     max: 0.009,
  //   },
  //   maxblur: 0.02,
  // });

  const { offset, darkness } = useControls({
    offset: 0.7,
    darkness: 1.5,
  });

  const aspect = useMemo(() => new Vector2(size.width, size.height), [size]);
  const vignetteSh = useMemo(
    () => VignetteShader(offset, darkness),
    [offset, darkness]
  );
  // const params = useMemo(
  //   () => ({
  //     focus,
  //     aperture,
  //     maxblur,
  //     width: size.with,
  //     height: size.height,
  //   }),
  //   [size, focus, aperture, maxblur]
  // );

  const hueShader = useMemo(
    () => HueSaturationShader(hue, saturation),
    [hue, saturation]
  );

  useEffect(() => composer.current.setSize(size.width, size.height), [size]);
  useFrame(() => {
    composer.current.render();
  }, 1);

  return (
    <effectComposer ref={composer} args={[gl]}>
      <renderPass attachArray="passes" scene={scene} camera={camera} />
      <unrealBloomPass
        attachArray="passes"
        args={[aspect, bloom, radius, treshold]}
      />
      <sAOPass
        attachArray="passes"
        args={[scene, camera, false, true, { x: size.width, y: size.height }]}
        params={{
          output: 0,
          saoBias: 0.0005,
          saoIntensity: 0.002,
          saoScale: 8,
          saoKernelRadius: 13,
          saoMinResolution: 0,
          saoBlur: true,
          saoBlurRadius: 8,
          saoBlurStdDev: 4,
          saoBlurDepthCutoff: 0.01,
        }}
      />
      <shaderPass attachArray="passes" args={[hueShader]} renderToScreen />
      <shaderPass attachArray="passes" args={[vignetteSh]} renderToScreen />
      {/* <bokehPass attachArray="passes" args={[scene, camera, params]} /> */}
      <shaderPass
        attachArray="passes"
        args={[FXAAShader]}
        material-uniforms-resolution-value={[1 / size.width, 1 / size.height]}
        renderToScreen
      />
    </effectComposer>
  );
}
