import { useLayoutEffect, useState } from "react";
import { useMediaQuery } from "react-responsive";

export default function BrowserBarActive() {
  const isTouchscreen = useMediaQuery({
    query: "(pointer:coarse)",
  });
  const isMobile = useMediaQuery({
    query: "(max-width: 1224px)",
  });

  return <>{isTouchscreen && isMobile && <ScrollOverlay />}</>;
}

function ScrollOverlay() {
  const [visible, setVisible] = useState(true);
  const [initialWait, setInitialWait] = useState(true);

  useLayoutEffect(() => {
    window.scrollTo(0, 0);
    setTimeout(() => setInitialWait(false), 1500);

    const handleScroll = () => {
      if (window.pageYOffset >= 30 && visible) {
        return setVisible(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
    // eslint-disable-next-line
  }, []);

  return (
    <>
      {visible && (
        <div
          className="absolute z-50 overflow-x-hidden overflow-y-hidden w-screen m-0 p-0
            h-[110vh] bg-black text-white flex justify-center items-center"
        >
          <div className="text-center">
            <div>
              <div className="text-5xl">Welcome</div>
              {!initialWait && (
                <div className="text-2xl mt-3 animate-pulse">
                  Scroll Down To Enter
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </>
  );
}
