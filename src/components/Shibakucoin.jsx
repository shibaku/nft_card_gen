import { useLayoutEffect, useRef } from "react";
import { useMemo } from "react";
import { Canvas, useLoader } from "@react-three/fiber";
import { FontLoader, Vector3 } from "three";
import { GLTFLoader } from "three-stdlib";
import { Suspense } from "react";
import { OrbitControls } from "@react-three/drei";
import SkyDome from "./SkyDome";

const response = {
  text: "bondage and yearning\nshiba with perfect manners\nsuch large animals",
  success: true,
};

const textArr = response.text.toUpperCase().split("\n");

function Text({ children, scale, ...props }) {
  const font = useLoader(FontLoader, "./shibacoin.json");
  const mesh = useRef();
  //
  useLayoutEffect(() => {
    const size = new Vector3();
    mesh.current.geometry.computeBoundingBox();
    mesh.current.geometry.boundingBox.getSize(size);
    mesh.current.position.x = -size.x / 2;
    mesh.current.position.y = -size.y / 2;
  }, [children]);

  const config = useMemo(
    () => ({
      font,
      size: 25,
      height: 1,
      curveSegments: 10,
      bevelEnabled: true,
      bevelThickness: 2,
      bevelSize: 1,
      // bevelOffset: 0,
      // bevelSegments: 8,
    }),
    [font]
  );
  return (
    <group scale={[scale, scale, scale]} {...props}>
      <mesh ref={mesh}>
        <textGeometry args={[children, config]} />
        {/* <meshLambertMaterial color={0xe15ed3} metalness={0.8} roughness={0.2} /> */}
        <meshNormalMaterial />
      </mesh>
    </group>
  );
}

export default function Haiku() {
  return (
    <div className="w-screen h-screen">
      <Canvas camera={[0.1]}>
        <ambientLight intensity={1} />
        <Suspense fallback={null}>
          <Text children={"124"} scale={0.012} position={[0, 0.5, 0]} />
          {/* <Text children={textArr[0]} scale={0.012} position={[0, 0.5, 0]} />
          <Text children={textArr[1]} scale={0.012} position={[0, 0, 0]} />
          <Text children={textArr[2]} scale={0.012} position={[0, -0.5, 0]} /> */}
          <SkyDome />
          <Head />
        </Suspense>
        <OrbitControls />
      </Canvas>
    </div>
  );
}

function Head() {
  const head = useLoader(GLTFLoader, "/head/headgltf.gltf");
  return <primitive object={head} />;
}
