import { useEffect } from "react";
import MenuGrid from "./MenuGrid";

function Home() {
  useEffect(() => {
    console.log(document.documentElement);
    document.documentElement.style.overflowY = "auto";
    document.querySelector("#root").style.overflowY = "auto";
  }, []);

  return (
    <div className="xl:h-screen xl:w-screen w-full  bg-[#03020F] grid  items-center">
      <div className="bg-[#03020F] pb-20">
        <img
          src="tribesdao.png"
          className="w-48 md:w-80 lg:w-72 2xl:w-96 mx-auto mb-2 md:mb-10 lg:mb-3"
          alt="tribesdao"
        />
        <MenuGrid />
      </div>
    </div>
  );
}

export default Home;
