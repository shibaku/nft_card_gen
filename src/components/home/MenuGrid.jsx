import { useState } from "react";
import { Link } from "react-router-dom";
import { useMapStore } from "store/useMapStore";

function MenuGrid() {
  const { menuItems } = useMapStore(["menuItems"], "app");
  const list = menuItems?.map((item, id) => <MenuItem item={item} key={id} />);

  return (
    <div className="grid md:grid-cols-1 gap-y-6 md:gap-y-16 lg:gap-y-11 lg:gap-x-  xl:gap-y-8 justify-items-center md:w-11/12  lg:w-10/12 mx-auto">
      {list}
    </div>
  );
}
export default MenuGrid;

function MenuItem({ item }) {
  const [hover, setHover] = useState(false);
  return (
    <Link
      className="relative"
      to={item?.path}
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <div
        className={`bg-gradient-to-b from-white to-[#E68285] p-[5px] rounded-2xl ${
          hover && "opacity-40"
        } shadow-white`}
      >
        <img
          src={item?.imgPath}
          key={item?.path}
          alt={item?.name}
          className="w-72 md:w-[30rem] lg:w-[35rem] xl:w-[24rem] 2xl:w-[30rem] rounded-2xl"
        />
      </div>
      {hover && (
        <p className="absolute text-xl xl:text-3xl top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 font-serif text-white text-center">
          {item?.name}
        </p>
      )}
    </Link>
  );
}
