import LoadStaticModel from "./LoadStaticModel";
import { useMapState } from "hooks/useMapState";

// rename mapState to useMapStore

export default function ModelListStatic() {
  const store = useMapState("staticModels");

  const ModelsComp = store.staticModels.map((model, index) => (
    <LoadStaticModel {...model} key={model.name} index={index} />
    // <ModelStatic path={model.path} key={model.name} visible={model.visible} />
  ));

  return <>{ModelsComp}</>;
}
