import { useMapState } from "hooks/useMapState";
import LoadStaticModel from "./LoadStaticModel";
import LoadModel from "./LoadModel";

export default function ModelList() {
  const { fileMaps, staticModelPaths } = useMapState(
    "fileMaps",
    "staticModelPaths"
  );

  const models = fileMaps.map((item, index) => (
    <LoadModel fileMap={item} key={index} />
  ));

  const modelsStatic = staticModelPaths.map((item, index) => (
    <LoadStaticModel {...item} key={item.name} index={index} />
  ));

  return (
    <>
      {fileMaps && models}
      {modelsStatic}
    </>
  );
}
