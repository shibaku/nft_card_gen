import { useLoader } from "@react-three/fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { useMapState } from "hooks/useMapState";
import { useEffect } from "react";
import traverseScene from "utils/traverseScene";
import { useThree } from "@react-three/fiber";

export default function LoadStaticModel({ path, position, visible, index }) {
  const { addModelGraph, updateNodeAttributes, setModelLoaded } = useMapState(
    "addModelGraph",
    "updateNodeAttributes",
    "setModelLoaded"
  );
  const { scene, asset } = useLoader(GLTFLoader, path);
  const { invalidate } = useThree();

  useEffect(() => {
    if (!scene) return;
    setModelLoaded();
    invalidate();
  }, [scene, invalidate, setModelLoaded]);

  useEffect(() => {
    if (scene) {
      const rootNode = {
        name: scene.name,
        type: scene.type,
        parent: scene.parent.name,
        visible: visible,
        graphIndex: [0],
      };
      scene.visible = visible;
      if (scene.children.length === 0) return;
      const sceneNodes = traverseScene(rootNode, scene.children, false);
      const name = asset.extras?.title || path.split(".")[0];
      addModelGraph(index, name, visible, sceneNodes);
    }
  }, [scene, index, asset.extras, addModelGraph, path, visible]);

  // todo: ideal would be if updateNodeAttributes
  // is already indexed to avoid re renders for other models
  // try to use selectors in Zustand and custom equality function
  useEffect(() => {
    updateNodeAttributes.forEach((update) => {
      if (update?.modelIndex === index) {
        const rootNode = scene;
        // build node query based on node index
        let node;
        update.graphIndex.forEach((el, index) => {
          if (index === 0) {
            node = rootNode;
          } else {
            node = node.children[el];
          }
        });
        node.visible = update.visible;
        // invalidate();
      }
    });
  }, [updateNodeAttributes, index, scene]);

  // todo: figure out why need to set visible to false to avoid change of visibility
  // on new model upload
  return (
    <>
      <primitive object={scene} position={position} visible={false} />
    </>
  );

  // todo: optimise how props are passed with something like this: <primitive object={scene} {...Object3dProps} />
  // refactor parent for this to pass props as path={} Object3dProps={...props}
}
