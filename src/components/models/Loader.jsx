import { useEffect } from "react";
import { useProgress } from "@react-three/drei";
import { useMapStore } from "store/useMapStore";

export default function Loader({ backgroundImg, disabled = false }) {
  const store = useMapStore(["setPreloader"]);
  const loader = useProgress();
  const { active, progress } = loader;

  useEffect(() => {
    store.setPreloader({
      active,
      progress,
      backgroundImg,
    });
    return () => {
      store.setPreloader({
        active: false,
      });
    };
  }, [store, progress, backgroundImg, active, loader]);

  if (disabled) return <></>;
  return <></>;
}
