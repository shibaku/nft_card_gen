import { useLoader } from "@react-three/fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

export default function LoadModel({ rootFile, fileMap }) {
  const objectURLs = [];

  const result = useLoader(GLTFLoader, fileMap.rootFile, (loader) => {
    loader.manager.setURLModifier((url) => {
      url = URL.createObjectURL(fileMap.map[url]);
      objectURLs.push(url);
      return url;
    });
    objectURLs.forEach((url) => URL.revokeObjectURL(url));
  });

  return <primitive object={result.scene} />;
}
