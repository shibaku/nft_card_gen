// install and test three-stlib
import { useRef, useEffect, useState } from "react";
import { DragControls } from "three/examples/jsm/controls/DragControls";
import { extend, useThree } from "@react-three/fiber";

extend({ DragControls });

export default function Draggable({ children }) {
  const [children3dObj, setChildren3dObj] = useState([]);
  const groupRef = useRef();
  const { camera, gl } = useThree();

  useEffect(() => {
    setChildren3dObj(groupRef.current.children);
    // console.log(groupRef.current);
  }, []);

  return (
    <group ref={groupRef}>
      <dragControls args={[children3dObj, camera, gl.domElement]} />
      {children}
    </group>
  );
}
