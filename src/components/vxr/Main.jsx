import { useRef, useEffect } from "react";
import { Canvas } from "@react-three/fiber";
import Scene from "./Scene";
import { useMapStore } from "store/useMapStore";
import Ui from "components/ui";
import { useLocation } from "react-router";
import Inputs from "components/inputs";
import Preloader from "./Preloader";
import useViewStoreFifo from "components/controllers/useViewStoreFifo";
import Audio from "components/controllers/Audio";
import useResponsive from "components/controllers/useResponsive";
import { resetStore } from "store/useAppStore";
import { resetStore as resetViewsStore } from "store/useViewsStore";
export default function ShibakuVerse() {
  // console.log("rerender main component");
  const ref = useRef();
  useViewStoreFifo();

  useEffect(() => {
    document.documentElement.style.overflowY = "hidden";
    document.body.overflowY = "hidden";
    document.querySelector("#root").style.overflowY = "hidden";
    // document.querySelector('#root').style.overflowY = "hidden";
  }, []);

  const { pathname } = useLocation();
  const viewKey = pathname.split("/")[1];

  const viewsStore = useMapStore([viewKey], "views");
  const thisView = viewsStore[viewKey];
  const { scene, inputs, audio } = thisView;
  const { position, near, far, fov } = scene.camera;
  const { frameloop } = scene.render;

  const device = useResponsive();

  useEffect(() => {
    return () => {
      resetStore();
      resetViewsStore();
    };
  }, []);

  return (
    <div className="xl:h-screen xl:w-screen w-full h-full z-10 overflow-hidden bg-black">
      <Canvas
        ref={ref}
        camera={{
          position: position[device],
          near,
          far,
          fov,
        }}
        frameloop={frameloop}
      >
        <Scene scene={scene} device={device} />
        {/* <Postprocessing intensity={0.2} threshold={0.5} smooth={0.1} /> */}
      </Canvas>
      <Preloader scene={scene} />
      <Ui ui={thisView.inputs.ui} />
      <Inputs inputs={inputs} />
      <Audio audio={audio} />
    </div>
  );
}
