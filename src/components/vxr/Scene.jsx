import { Suspense } from "react";
import { Environment, Stats } from "@react-three/drei";
import Preloader from "./SuspensePreloader";
import Gltfs from "./Gltfs";
import SceneElements from "../three/ThreeElements";
import Controllers from "components/controllers/Controllers";
// import { useThree } from "@react-three/fiber";

export default function Scene({ scene, device }) {
  //  todo: make this conditional based on framerate minimum or gpu detection
  // const setPixelRatio = useThree((state) => state.setDpr);
  // const current = useThree((state) => state.performance.current);
  // console.log(current);
  // useEffect(() => {
  //   setPixelRatio(window.devicePixelRatio * 0.5);
  // }, [current, setPixelRatio]);

  return (
    <>
      <pointLight intensity={1} position={[200, 200, 200]} />
      <Suspense fallback={<Preloader {...scene.preloader} />}>
        {/* <Suspense fallback={null}> */}
        <Gltfs gltfs={scene.gltfs} />
        <Environment path={"/"} files={scene.environment} />
      </Suspense>
      <SceneElements elements={scene.elements} />
      <Controllers scene={scene} device={device} />
      {/* <Postprocessing /> */}
      {scene?.tools?.stats && <Stats />}
    </>
  );
}
