import { useEffect, useRef } from "react";
import { useMapStore } from "store/useMapStore";

export default function useAddMixerEvents(mixer) {
  const firstRender = useRef(true);
  const { updateGltfAnimation } = useMapStore(["updateGltfAnimation"], "app");

  useEffect(() => {
    function updateAnimStateOnFinish(e) {
      updateGltfAnimation(e.action._clip.name, { running: false });
    }

    if (firstRender.current && mixer) {
      mixer.addEventListener("finished", updateAnimStateOnFinish);
      firstRender.current = false;
    }
    return () => {
      mixer.removeEventListener("finished", updateAnimStateOnFinish);
    };
  }, [mixer, updateGltfAnimation]);
}
