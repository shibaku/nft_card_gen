import { useRef, useEffect } from "react";
import { useGLTF, useAnimations } from "@react-three/drei";
import { useMapStore } from "store/useMapStore";
import { LoopOnce } from "three";
import useAddMixerEvents from "./useAddMixerEvents";
import SceneObjects from "three/sceneObjects";
import useEnableTransformControls from "components/three/useEnableTransformControls";

export default function Gltfs({ gltfs }) {
  const models = gltfs.map((model, index) => {
    return <Model model={model} key={index} />;
  });

  return <>{models}</>;
}

function Model({ model }) {
  const primitive = useRef();
  const { addTransformControls, removeTransformControls } =
    useEnableTransformControls(primitive, model.modelName);

  const { addModelNameToAnim } = useMapStore(["addModelNameToAnim"], "app");
  addModelNameToAnim(model.modelName);

  const position = model?.position || [0, 0, 0];
  const scale = model?.scale || 1;
  const rotation = model?.rotation || [0, 0, 0];

  const { scene, animations } = useGLTF(model.path, "/draco/");
  useAddToSceneObjects(model.modelName, scene);
  const { actions, mixer } = useAnimations(animations, primitive);
  useAddMixerEvents(mixer);

  // play animations after load
  useEffect(() => {
    // const actionsList = Object.keys(actions); //memorize this
    // actionsList.forEach((action) => actions[action]?.play());
    model.playAnimations?.forEach((action) => actions[action]?.play());
  }, [actions, model]);

  function onClick() {
    if (model.enableTransforms) addTransformControls();
  }
  function onPointerMissed() {
    if (model.enableTransforms) removeTransformControls();
  }

  return (
    <>
      <primitive
        ref={primitive}
        object={scene}
        position={position}
        scale={scale}
        rotation={rotation}
        onClick={onClick}
        onPointerMissed={onPointerMissed}
      />
      <Animations actions={actions} mixer={mixer} modelName={model.modelName} />
    </>
  );
}

function Animations({ actions, modelName, mixer }) {
  // !use zustand selector useStore(state => gltfAnimations[modelName])
  const { gltfAnimations, updateGltfAnimation } = useMapStore(
    ["gltfAnimations", "updateGltfAnimation"],
    "app"
  );

  useEffect(() => {
    Object.entries(gltfAnimations[modelName]).forEach(([key, value]) => {
      // ! add error catch for missing keys
      const thisAction = actions[key];

      if (!value.active || value?.running) return;
      console.log(thisAction._clip.name);
      if (value.completed)
        if (value?.effectiveTimeScale)
          thisAction.setEffectiveTimeScale(value.effectiveTimeScale);
      if (!value.loop) {
        thisAction.reset();
        thisAction.setLoop(LoopOnce);
        updateGltfAnimation(key, { running: true, active: false });
      }
      if (value?.startAt) {
        thisAction.startAt(value.startAt + mixer.time);
      }
      thisAction.play();
    });
  }, [gltfAnimations, modelName, actions, mixer.time, updateGltfAnimation]);
  return <></>;
}

function useAddToSceneObjects(modelName, scene) {
  const added = useRef(false);
  useEffect(() => {
    if (!scene || added.current) return;
    SceneObjects.addScene(modelName, scene);
    return () => SceneObjects.removeScene(modelName);
  }, [scene, modelName]);
}
