import { useThree } from "@react-three/fiber";
import { useEffect } from "react";
import { useControls } from "leva";
import { useCreateSphereCoord } from "hooks/useCreateCoord";

const deg = Math.PI / 180;

export default function CameraRenderSettings() {
  const sphereCoord = useCreateSphereCoord();
  const { camSettings, camLookAtY, exposure, camPos, camPhi, camStepSize } =
    useControls({
      camSettings: { fov: 81, near: 0.001, far: 2000 },
      camLookAtY: {
        min: 0.05,
        max: 0.09,
        value: 0.068,
        step: 0.001,
      },
      exposure: {
        value: 0.5,
        min: 0,
        max: 1,
        step: 0.05,
      },
      camPos: {
        x: 0,
        y: 0.09,
        z: 0.12,
      },
      camPhi: 0,
      camStepSize: 1,
    });
  const { camera, gl, invalidate } = useThree();

  useEffect(() => {
    if (!sphereCoord) return;
    const dTheta = deg * camPhi * camStepSize;
    sphereCoord.theta = dTheta;
    camera.position.setFromSpherical(sphereCoord);
    camera.lookAt(0, camLookAtY, 0);
    camera.updateProjectionMatrix();
    invalidate();
  });

  useEffect(() => {
    if (!sphereCoord) return;
    sphereCoord.setFromCartesianCoords(camPos.x, camPos.y, camPos.z);
    camera.fov = camSettings.fov;
    camera.near = camSettings.near;
    camera.far = camSettings.far;
    camera.position.setFromSpherical(sphereCoord);
    camera.lookAt(0, camLookAtY, 0);
    camera.updateProjectionMatrix();
    gl.toneMappingExposure = exposure;
    invalidate();
  }, [
    camSettings,
    camera,
    exposure,
    gl,
    camPos,
    camLookAtY,
    sphereCoord,
    invalidate,
  ]);

  return <></>;
}
