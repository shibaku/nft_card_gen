import { useThree } from "@react-three/fiber";
import { useEffect } from "react";
import { useControls } from "leva";
import { OrbitControls } from "@react-three/drei";

export default function CameraController({
  lookAt = [0, 0, 0],
  position = [0, 0, 0],
  ...props
}) {
  const { camFrustum, camPosition, orbitLookAt } = useControls({
    camFrustum: { fov: 50, near: 0.1, far: 2000 },
    camPosition: {
      x: position[0],
      y: position[1],
      z: position[2],
    },
    orbitLookAt: {
      x: lookAt[0],
      y: lookAt[1],
      z: lookAt[2],
    },
  });
  const { camera, gl } = useThree();

  useEffect(() => {
    camera.fov = camFrustum.fov;
    camera.near = camFrustum.near;
    camera.far = camFrustum.far;
    camera.position.x = camPosition.x;
    camera.position.y = camPosition.y;
    camera.position.z = camPosition.z;
    camera.updateProjectionMatrix();
  }, [camFrustum, camera, gl, camPosition]);

  return <OrbitControls target={Object.values(orbitLookAt)} />;
}
