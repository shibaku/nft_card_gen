import { useThree } from "@react-three/fiber";
import { useEffect } from "react";
import { useControls } from "leva";

export default function CameraSettings() {
  const { camSettings, exposure, camPos } = useControls({
    camSettings: { fov: 50, near: 0.1, far: 2000 },
    exposure: {
      value: 0.2,
      min: 0,
      max: 1,
      step: 0.01,
    },
    camPos: {
      x: 0,
      y: 0.05,
      z: 0.24,
    },
  });
  const { camera, gl } = useThree();

  useEffect(() => {
    camera.fov = camSettings.fov;
    camera.near = camSettings.near;
    camera.far = camSettings.far;
    camera.position.x = camPos.x;
    camera.position.y = camPos.y;
    camera.position.z = camPos.z;
    camera.updateProjectionMatrix();
    gl.toneMappingExposure = exposure;
  }, [camSettings, camera, exposure, gl, camPos]);

  return <></>;
}
