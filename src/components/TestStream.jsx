import { useEffect, useRef } from "react";
// import dashjs from "dashjs";
import Hls from "hls.js";

export default function TestStream() {
  const vidRef = useRef();

  // function iOS() {
  //   return (
  //     [
  //       "iPad Simulator",
  //       "iPhone Simulator",
  //       "iPod Simulator",
  //       "iPad",
  //       "iPhone",
  //       "iPod",
  //     ].includes(navigator.platform) ||
  //     // iPad on iOS 13 detection
  //     (navigator.userAgent.includes("Mac") && "ontouchend" in document)
  //   );
  // }

  useEffect(() => {
    const vidEl = vidRef.current;
    vidEl.autoPlay = true;
    vidEl.loop = true;
    vidEl.muted = true;
    vidEl.playsInline = true;
    vidEl.crossorigin = "anonymous";

    const isIOS = true;

    let player;
    if (isIOS) {
      if (Hls.isSupported()) {
        const config = {
          manifestLoadingTimeOut: 20000,
          levelLoadingTimeOut: 20000,
          fragLoadingTimeOut: 20000,
          nudgeOffset: 0,
          nudgeMaxRetry: 10,
          loop: true,
        };
        player = new Hls(config);
        player.attachMedia(vidEl);
        player.on(Hls.Events.MEDIA_ATTACHED, function () {
          player.loadSource(
            "https://videodelivery.net/e77730d30974f44f015c8c8b0e4f8733/manifest/video.m3u8"
          );
        });
        player.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
          // console.log("hls manifest parsed");
          vidEl.play();
        });
        player.on(Hls.Events.ERROR, function (event, data) {
          // console.log(event);
          // console.log(data);
          switch (data.type) {
            case Hls.ErrorTypes.NETWORK_ERROR:
              console.log("fatal network error encountered, try to recover");
              player.startLoad();
              break;
            case Hls.ErrorTypes.MEDIA_ERROR:
              console.log("fatal media error encountered, try to recover");
              // player.swapAudioCodec();
              // player.lastCurrentTime = 0;
              // player.currentTime = 0;
              // player.recoverMediaError();
              // setCanPlay(false);
              break;
            default:
              // cannot recover
              player.destroy();
              break;
          }
        });
      } else if (vidEl.canPlayType("application/vnd.apple.mpegurl")) {
        vidEl.src =
          "https://videodelivery.net/e77730d30974f44f015c8c8b0e4f8733/manifest/video.m3u8";
        vidEl.play();
        // vidEl.addEventListener("loadedmetadata", function () {
        //   console.log("loadedmetadata");
        //   vidEl.play();
        // });
      }
    }
  }, []);

  return (
    <div>
      <video ref={vidRef} className="w-96 h-96" />
    </div>
  );
}

// if (!isIOS) {
//   player = dashjs.MediaPlayer().create();
//   player.initialize(vidEl, src.dash, false);
//   // player.updateSettings({
//   //   streaming: {
//   //     jumpGaps: true,
//   //     jumpLargeGaps: true,
//   //     smallGapLimit: 0.2,
//   //   },
//   // });
//   player.on("error", (e) => console.log(e));
// }
