import { useMapStore } from "store/useMapStore";
import SceneObjects from "three/sceneObjects";

export default function useEnableTransformControls(objRef, name) {
  const { transFormController, setTransFormController } = useMapStore(
    ["transFormController", "setTransFormController"],
    "app"
  );

  function addTransformControls() {
    const prevState = transFormController;
    if (prevState.name === name) return;
    SceneObjects.setTransformObj(objRef.current);
    setTransFormController({ active: true, name });
  }

  function removeTransformControls() {
    const prevState = transFormController;
    if (!(prevState.name === name)) return;
    setTransFormController({ active: false, name: null });
    SceneObjects.removeTransformObj();
  }
  return { addTransformControls, removeTransformControls };
}
