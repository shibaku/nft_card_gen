import { useEffect, useRef, useState } from "react";
import { useTexture } from "@react-three/drei";
import { Suspense } from "react/cjs/react.production.min";
import dashjs from "dashjs";
import { Html } from "@react-three/drei";
// import Hls from "hls.js";
// import videojs from "video.js";
// import { useAspect } from "@react-three/drei";

export default function Material({ type, distanceThreshold, ...props }) {
  if (type === "video")
    return <Video distanceThreshold={distanceThreshold} {...props} />;
}

function Video({ distanceThreshold, side = "single", src, stream }) {
  const videoRef = useRef({ vidEl: null, player: null });

  const [initialLoad, setInitialLoad] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const [canPlay, setCanPlay] = useState(false);

  const thumbnail = useTexture(src.thumbnail);

  useEffect(() => {
    if (!initialLoad) return null;

    const video = videoRef.current;

    video.vidEl = document.createElement("video");
    video.vidEl.autoPlay = true;
    video.vidEl.loop = true;
    video.vidEl.muted = true;
    video.vidEl.playsInline = true;
    video.vidEl.crossOrigin = "anonymous";

    if (
      (video.vidEl.canPlayType("application/vnd.apple.mpegurl") ||
        video.vidEl.canPlayType("audio/mpegurl")) &&
      isIOS()
    ) {
      video.vidEl.src = src.hls;
      video.vidEl.play();
      // vidEl.addEventListener("loadedmetadata", function () {
      // setCanPlay(true);
      // });
    } else if (dashjs.supportsMediaSource()) {
      video.player = dashjs.MediaPlayer().create();
      video.player.initialize(video.vidEl, src.dash, false);
      video.player.updateSettings({
        streaming: {
          jumpGaps: true,
          jumpLargeGaps: true,
          smallGapLimit: 0.2,
        },
      });
      video.player.on("error", (e) => console.log(e));
      video.vidEl.play();
    } else {
      throw new Error("Video streaming not supported");
    }

    video.vidEl.oncanplay = () => {
      if (canPlay) return;
      setCanPlay(true);
    };

    return () => {
      if (video.player) video.player.destroy();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [src, initialLoad]);

  useEffect(() => {
    if (distanceThreshold && !initialLoad) {
      setInitialLoad(true);
      return;
    }
    if (distanceThreshold && canPlay) {
      videoRef.current.vidEl.play();
      setLoaded(true);
      return;
    }
    if (!distanceThreshold && canPlay) {
      videoRef.current.vidEl.currentTime = 0;
      videoRef.current.vidEl.pause();
    }
  }, [distanceThreshold, src, loaded, canPlay, initialLoad]);

  // need to do cleanup in separate side Effect
  // to avoid premature cleanup call
  // useEffect(() => {
  //   return () => {
  //     if (video.player) video.player.destroy();
  //   };
  // }, [video?.player]);

  const matSide = side === "DoubleSide" ? 2 : 0;

  if (loaded && canPlay && initialLoad)
    return (
      <>
        <meshBasicMaterial side={matSide}>
          <videoTexture attach="map" args={[videoRef.current.vidEl]} />
        </meshBasicMaterial>
      </>
    );
  else
    return (
      <Suspense>
        <meshBasicMaterial side={matSide} map={thumbnail} />;
        <Html
          position={[0, 0, 0]}
          prepend
          // sprite
          center
          transform
        >
          <>
            {initialLoad && (
              <p className="bg-gray-100 animate-pulse w-24 text-center text-xl rounded-2xl select-none">
                Loading
              </p>
            )}
          </>
        </Html>
      </Suspense>
    );
}

// ! check if is iphone
function isIOS() {
  return (
    [
      "iPad Simulator",
      "iPhone Simulator",
      "iPod Simulator",
      "iPad",
      "iPhone",
      "iPod",
    ].includes(navigator.platform) ||
    // iPad on iOS 13 detection
    (navigator.userAgent.includes("Mac") && "ontouchend" in document)
  );
}

// ! check for native hls support
// else if (vidEl.canPlayType("application/vnd.apple.mpegurl"))

// !HLS.js
// if (isIOS) {
//   if (Hls.isSupported()) {
//     const config = {
//       manifestLoadingTimeOut: 20000,
//       levelLoadingTimeOut: 20000,
//       fragLoadingTimeOut: 20000,
//       nudgeOffset: 0,
//       nudgeMaxRetry: 10,
//       loop: true,
//     };
//     player = new Hls(config);
//     player.attachMedia(vidEl);
//     player.on(Hls.Events.MEDIA_ATTACHED, function () {
//       player.loadSource(src.hls);
//     });
//     player.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
//       setCanPlay(true);
//     });
//     player.on(Hls.Events.ERROR, function (event, data) {
//       switch (data.type) {
//         case Hls.ErrorTypes.NETWORK_ERROR:
//           console.log("fatal network error encountered, try to recover");
//           setCanPlay(false);
//           player.startLoad();
//           break;
//         case Hls.ErrorTypes.MEDIA_ERROR:
//           console.log("fatal media error encountered, try to recover");
//           // player.swapAudioCodec();
//           // player.lastCurrentTime = 0;
//           // player.currentTime = 0;
//           // player.recoverMediaError();
//           // setCanPlay(false);
//           break;
//         default:
//           // cannot recover
//           player.destroy();
//           break;
//       }
//     });

// !Video Js
// player = videojs(
//   vidEl,
//   {
//     // autoplay: true,
//     controls: false,
//     loop: true,
//     responsive: false,
//     fluid: true,
//     sources: [
//       {
//         src: src.hls,
//         // type: "application/dash+xml",
//       },
//     ],
//   },
//   () => {
//     // console.log("player is ready");
//     // onReady && onReady(player);
//   }
// );
