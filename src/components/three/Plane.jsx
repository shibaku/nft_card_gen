import Material from "components/three/Material";
import { useRef, useState } from "react";
import { useFrame } from "@react-three/fiber";
import useEnableTransformControls from "./useEnableTransformControls";
import { Suspense } from "react/cjs/react.production.min";
import { useMapStore } from "store/useMapStore";

export default function Plane({ config, name }) {
  const [distanceThreshold, setDistanceThreshold] = useState(false);
  const ref = useRef();
  const { position, rotation, width, height, material } = config;

  const { preloader } = useMapStore(["preloader"]);
  const isActive = preloader?.active || false;

  const { addTransformControls, removeTransformControls } =
    useEnableTransformControls(ref, name);

  useFrame(({ camera }) => {
    // todo: add config entry to activate/deactivate as well as distance threshold
    if (!ref.current?.position) return;
    const distance = camera.position.distanceTo(ref.current.position);
    if (distance > 25 && distanceThreshold) setDistanceThreshold(false);
    if (distance < 25 && !distanceThreshold) setDistanceThreshold(true);
  });
  // console.log(distanceThreshold);

  const plane = (
    <Suspense fallback={""}>
      <mesh
        position={position}
        rotation={rotation}
        ref={ref}
        onClick={addTransformControls}
        onPointerMissed={removeTransformControls}
      >
        <planeGeometry args={[width, height]} />
        <Material {...material} distanceThreshold={distanceThreshold} />
      </mesh>
    </Suspense>
  );

  if (isActive) return <></>;
  return <>{plane}</>;
}
