import SkyDome from "./SkyDome";
import Plane from "./Plane";

export default function SceneElements({ elements }) {
  // console.log(elements);
  const el = Object.entries(elements).map(([key, value]) =>
    mapType(value.type, value.config, key)
  );

  return <>{el}</>;
}

function mapType(type, config, key) {
  const attr = {
    config,
    name: key,
    key: key,
  };

  switch (type) {
    case "plane":
      return <Plane {...attr} />;
    case "skyDome":
      return <SkyDome {...attr} />;
    default:
      return <></>;
  }
}
