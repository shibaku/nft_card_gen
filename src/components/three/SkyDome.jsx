import { useControls } from "leva";
import { BackSide, TextureLoader } from "three";
import { useLoader } from "@react-three/fiber";
import { Suspense } from "react";

export default function SkyDome({ config }) {
  if (config.controls)
    return (
      <Suspense fallback={""}>
        <WithControls config={config} />
      </Suspense>
    );
  return (
    <Suspense fallback={""}>
      <WithProps config={config} />
    </Suspense>
  );
}

function WithControls({
  config: {
    radius,
    widthSegments,
    heightSegments,
    emission,
    texture,
    lightMap,
  },
}) {
  const { skyDome } = useControls({
    skyDome: { radius, widthSegments, heightSegments },
  });

  const { domeEmission } = useControls({
    domeEmission: {
      value: emission,
      min: 0,
      max: 4,
      step: 0.05,
    },
  });

  const configUpdate = {
    ...skyDome,
    emission: domeEmission,
    texture,
    lightMap,
  };

  return <WithProps config={configUpdate} />;
}

function WithProps({
  config: {
    radius,
    widthSegments,
    heightSegments,
    emission,
    texture,
    lightMap,
  },
}) {
  const map = useLoader(TextureLoader, texture);
  const mapIntensity = useLoader(TextureLoader, lightMap);
  // todo: make configurable side: double or inside
  return (
    <mesh>
      <sphereGeometry args={[radius, widthSegments, heightSegments]} />
      <meshBasicMaterial
        side={BackSide}
        map={map}
        lightMap={mapIntensity}
        lightMapIntensity={emission}
      />
    </mesh>
  );
}
