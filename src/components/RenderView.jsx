import { Suspense } from "react";
import { Canvas } from "@react-three/fiber";
import { Environment } from "@react-three/drei";
import ModelList from "./models/ModelList";
import CameraRenderControl from "./CameraRenderControl";
// import SkyDomeRender from "./SkyDomeRender";
import PuppeteerControls from "./PuppeteerControls";
import {
  camera,
  pointLight,
  ambientLight,
  hdr,
} from "../data/renderSettigs.json";
import PostprocessingRender from "./PostprocessingRender";
import { useMapState } from "hooks/useMapState";

export default function RenderView() {
  const store = useMapState("metaData");
  return (
    <div className="w-screen absolute top-0 left-0 h-screen">
      <div className="w-full h-full">
        <Canvas
          frameloop="demand"
          camera={{
            position: [camera.x, camera.y, camera.z],
            fov: camera.fov,
            near: camera.near,
            far: camera.far,
          }}
          gl={{ antialias: true, alpha: false }}
          // gl={{ toneMappingExposure: 0.1 }}
        >
          <CameraRenderControl />
          <ambientLight intensity={ambientLight} />
          <pointLight intensity={pointLight} position={[0, 10, 4]} />
          <pointLight intensity={pointLight} position={[10, -10, 4]} />
          <pointLight
            intensity={pointLight}
            position={[-10, -10, 0]}
            color="blue"
          />
          {store.metaData && (
            <Suspense fallback={""}>
              <ModelList />
            </Suspense>
          )}
          <Suspense fallback={null}>
            <Environment files={hdr} />
          </Suspense>
          {/* <Suspense fallback={""}>
            <SkyDomeRender />
          </Suspense> */}
          <PostprocessingRender />
        </Canvas>
      </div>
      <PuppeteerControls />
    </div>
  );
}
