import { Suspense } from "react";
import { useControls } from "leva";
import { Canvas } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";
import { Environment } from "@react-three/drei";
import ModelList from "./models/ModelList";
// import CameraSettingsEditor from "./CameraSettingsEditor";
import CameraRenderSettings from "./CameraRenderSettings";
import RenderWithMetadata from "./RenderWithMetadata";
import { camera, hdr } from "../data/renderSettigs.json";
import PostprocessingCustom from "./PostprocessingCustom";
import { useMapState } from "hooks/useMapState";

export default function RenderSettingsView() {
  const store = useMapState("metaData");
  const { orbitC, lookAtOrbitC } = useControls({
    orbitC: false,
    lookAtOrbitC: 0.068,
  });
  const { pointLight, ambientLight } = useControls({
    pointLight: 2.19,
    ambientLight: 3.5,
  });
  return (
    <div className="w-screen absolute top-0 left-0 h-screen">
      <div className="w-full h-full">
        <Canvas
          frameloop="demand"
          camera={{
            position: [camera.x, camera.y, camera.z],
            fov: camera.fov,
            near: camera.near,
            far: camera.far,
          }}
          gl={{ antialias: true, alpha: false }}
          // gl={{ toneMappingExposure: 0.1 }}
        >
          <ambientLight intensity={ambientLight} />
          <pointLight intensity={pointLight} position={[0, 10, 4]} />
          <pointLight intensity={pointLight} position={[10, -10, 4]} />
          <pointLight
            intensity={pointLight}
            position={[-10, -10, 0]}
            color="blue"
          />
          {store.metaData && (
            <Suspense fallback={""}>
              <ModelList />
            </Suspense>
          )}
          <Suspense fallback={null}>
            <Environment files={hdr} />
          </Suspense>
          {/* <CameraSettingsEditor /> */}
          <CameraRenderSettings />
          <PostprocessingCustom />
          {orbitC && <OrbitControls target={[0, lookAtOrbitC, 0]} />}
        </Canvas>
      </div>
      <RenderWithMetadata />
    </div>
  );
}
