import useKeyboardInput from "./useKeyboardInput";
import InputControllers from "./InputControllers";
import Joystick from "./Joystick";

export default function InputsMain({ inputs }) {
  useKeyboardInput();
  // useInputControllers(inputs);
  return (
    <>
      <InputControllers inputs={inputs} />
      {inputs?.joysticks && <Joysticks joysticks={inputs.joysticks} />}
    </>
  );
}

function Joysticks({ joysticks }) {
  const components = Object.entries(joysticks).map(([key, value]) => {
    if (value.active)
      return (
        <Joystick
          key={key}
          config={value.config}
          active={value.active}
          position={value.config?.position}
          id={key}
          name={key}
        />
      );
    return null;
  });
  return <>{components}</>;
}
