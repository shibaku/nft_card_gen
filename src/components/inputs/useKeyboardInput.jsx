import { useEffect, useRef } from "react";
import { useMapStore } from "store/useMapStore";
import produce from "immer";

export default function useKeyboardInput() {
  const { setKeyboardEvents } = useMapStore(["setKeyboardEvents"], "inputs");

  // useRef for locale storage as keyboardEvents won't pass without rerender
  const keyEvents = useRef({});

  // todo:change to useMemo?
  useEffect(() => {
    const handleKeyDown = (event) => {
      const keyName = event.key;
      if (keyEvents.current[keyName]?.active) {
        return;
      }
      keyEvents.current = produce(keyEvents.current, (draft) => {
        draft[keyName] = {
          active: true,
          event,
        };
      });
      setKeyboardEvents(keyEvents.current);
    };

    const handleKeyUp = (event) => {
      const keyName = event.key;
      keyEvents.current = produce(keyEvents.current, (draft) => {
        draft[keyName] = {
          active: false,
          event,
        };
      });
      setKeyboardEvents(keyEvents.current);
      const updateKeyEvents = produce(keyEvents.current, (draft) => {
        delete draft[keyName];
      });
      keyEvents.current = updateKeyEvents;
    };
    window.addEventListener("keydown", handleKeyDown);
    window.addEventListener("keyup", handleKeyUp);

    return () => {
      window.removeEventListener("keydown", handleKeyDown);
      window.removeEventListener("keyup", handleKeyUp);
    };
    // eslint-disable-next-line
  }, []);
}
