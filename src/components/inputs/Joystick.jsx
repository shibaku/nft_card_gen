import { useRef, useEffect } from "react";
import { useMapStore } from "store/useMapStore";
import nipplejs from "nipplejs";
import produce from "immer";

export default function Joystick({ config, active, name = "joystick" }) {
  const { position = "left" } = config;
  const joyRef = useRef();
  const joyStateRef = useRef({});
  const joystick = useRef(null);

  const { setJoystickEvents } = useMapStore(["setJoystickEvents"], "inputs");

  //create event store for current joystick component on first render
  useEffect(() => {
    setJoystickEvents(name, joyStateRef.current);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (joystick.current) return;
    joystick.current = nipplejs.create({
      zone: joyRef.current,
      color: "white",
      mode: "static",
      position: {
        top: "50%",
        left: "50%",
      },
      // multitouch: true,
    });

    // todo: wrap in memo or useCallback
    const updateState = (stateRef, dir, active) => {
      stateRef.current = produce(stateRef.current, (draft) => {
        draft[dir] = {
          active: active,
        };
      });
      setJoystickEvents(name, stateRef.current);
      if (!active) {
        const updateState = produce(stateRef.current, (draft) => {
          delete draft[dir];
        });
        stateRef.current = updateState;
      }
    };

    const updateDir = (vectorComp, stateRef, dirs, threshold) => {
      // Activate
      if (vectorComp > threshold && !stateRef.current[dirs.posDir]) {
        updateState(stateRef, dirs.posDir, true);
      }
      if (vectorComp < -threshold && !stateRef.current[dirs.negDir]) {
        updateState(stateRef, dirs.negDir, true);
      }
      // Release
      if (vectorComp < threshold && stateRef.current[dirs.posDir]) {
        updateState(stateRef, dirs.posDir, false);
      }
      if (vectorComp > -threshold && stateRef.current[dirs.negDir]) {
        updateState(stateRef, dirs.negDir, false);
      }
    };

    joystick.current
      .on("move", function (evt, nipple) {
        updateDir(
          nipple.vector.y,
          joyStateRef,
          { posDir: "forward", negDir: "backward" },
          0.25
        );
        updateDir(
          nipple.vector.x,
          joyStateRef,
          { posDir: "right", negDir: "left" },
          0.25
        );
      })
      .on("end", function (evt, nipple) {
        const dirs = Object.keys(joyStateRef.current);
        if (dirs.length === 0) return;
        const newState = {};
        dirs.forEach((dir) => {
          newState[dir] = {
            active: false,
          };
        });
        setJoystickEvents(name, newState);
        joyStateRef.current = {};
      })
      .on("removed", function (evt, nipple) {
        nipple.off("start move end dir plain");
      });

    return () => {
      joystick.current.destroy();
    };
  }, [setJoystickEvents, name]);

  // todo: can remove active check and only do css size detection
  const isActive = active ? "inline xl:hidden" : "hidden";

  if (position === "left")
    return (
      <div
        className={`absolute bottom-16 left-16 z-40 ${isActive}`}
        ref={joyRef}
      ></div>
    );
  if (position === "right")
    return (
      <div
        className={`absolute bottom-16 right-16 z-40 ${isActive}`}
        ref={joyRef}
      ></div>
    );
}
