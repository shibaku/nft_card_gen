import { useMapStore } from "store/useMapStore";
import { useEffect } from "react";
import { produce } from "immer";

//todo: current system creates double triggers
// for pressing two keys at the same time as well as
// joystick dual directions due to structure of inputEvenState
// which has more then one input on second event trigger
// so first trigger is reprocessed as still in eventState
// possible solution: add second control parameter after action has been triggered
// ignore events with additional parameter on subsequent execs

// todo: whole component rerenders for either keyboard or joystick

export default function InputControllers({ inputs }) {
  useTriggerKeyboardActions(inputs?.keyboard);
  // useTriggerJoystickActions(inputs?.joysticks);

  if (inputs?.joysticks)
    return <TriggerJoystickActions joysticks={inputs.joysticks} />;
  return null;
}

function TriggerJoystickActions({ joysticks }) {
  const { setControllers } = useMapStore(["setControllers"], "app");

  //joystick event stores are created dynamically, based on config
  // get all joystick names and subscribe to their eventStores
  const joystickKeys = Object.keys(joysticks);
  const joysticksEventsStates = useMapStore(joystickKeys, "inputs");
  const { setJoystickEvents } = useMapStore(["setJoystickEvents"], "inputs");

  useEffect(() => {
    // loop through all joystick entities
    joystickKeys.forEach((joystickName) => {
      const joystickEvents = joysticksEventsStates[joystickName]; //single eventStore
      const joystickEvConf = joysticks[joystickName]; //event & action defs for this joystick

      //loop through all events of this joystick
      //todo: refactor into function
      if (joystickEvents) {
        Object.entries(joystickEvents).forEach(([eventKey, value]) => {
          if (joystickEvConf[eventKey]) {
            // create payload for setControllers with updated actions for current event
            const update = produce(
              joystickEvConf[eventKey].actions,
              (draft) => {
                draft.forEach((action) => {
                  action.state.active = value.active;
                });
              }
            );
            if (update.length > 0) setControllers(update);
            removeJoystickEvent(
              joystickName,
              eventKey,
              value,
              joystickEvents,
              setJoystickEvents
            );
          }
        });
      }
    });
  }, [
    joysticksEventsStates,
    joysticks,
    setControllers,
    joystickKeys,
    setJoystickEvents,
  ]);
  return null;
}

function useTriggerKeyboardActions(keyInputs) {
  const { keyboardEvents, setKeyboardEvents } = useMapStore(
    ["keyboardEvents", "setKeyboardEvents"],
    "inputs"
  );
  const { setControllers } = useMapStore(["setControllers"], "app");

  useEffect(() => {
    Object.entries(keyboardEvents).forEach(([key, value]) => {
      // key not in view inputs list, garbage collect event
      if (!keyInputs?.[key]) {
        removeKeyEvent(key, value, keyboardEvents, setKeyboardEvents);
        return;
      }

      // key in list
      const keyActions = keyInputs[key].actions;
      // only allow keys actions with correct event type
      const filteredActions = keyActions.filter((action) => {
        if (
          action?.config?.type &&
          !(action?.config?.type === value.event.type)
        )
          return false;
        return true;
      });
      // update states, call controllers for each match action
      const update = produce(filteredActions, (draft) => {
        draft.forEach((action) => {
          action.state.active = value.active;
          action.state.event = value.event;
        });
      });
      if (update.length > 0) setControllers(update);

      // remove key from event list if not active
      // todo: can this be done in useKeyboardInput? as done in useJoystickInput?
      removeKeyEvent(key, value, keyboardEvents, setKeyboardEvents);
    });
  }, [keyboardEvents, keyInputs, setControllers, setKeyboardEvents]);
}

function removeKeyEvent(key, value, keyboardEvents, setKeyboardEvents) {
  if (!value.active) {
    const updateKeyEvents = produce(keyboardEvents, (draft) => {
      delete draft[key];
    });
    setKeyboardEvents(updateKeyEvents);
  }
}

function removeJoystickEvent(
  stateName,
  eventKey,
  value,
  joystickEvents,
  setJoystickEvents
) {
  if (!value.active) {
    const eventsUpdate = produce(joystickEvents, (draft) => {
      delete draft[eventKey];
    });
    setJoystickEvents(stateName, eventsUpdate);
  }
}
