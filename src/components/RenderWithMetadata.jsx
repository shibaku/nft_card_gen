import { useState, useMemo } from "react";
import { useMapState } from "hooks/useMapState";
import mapCSV from "data/mapMeta.csv";
import mapHaikuCsv from "data/mapHaiku.csv";
import mapException from "data/mapMetaExceptions.csv";
import mapMetaToStore from "data/mapMetaToStore.csv";
import { sampleMeta } from "data/sampleMetadata";

import {
  mapCsvToObj,
  exceptionCsvToObj,
  metaDataCsvToObj,
  getHaikuNodes,
  tokenIdDigits,
  mapMetaToStoreCsvToObj,
} from "utils/metadataUtils";

// move these into metadataUtils
export default function RenderWithMetadata() {
  const exceptions = useMemo(() => exceptionCsvToObj(mapException), []);
  const mapMeta = useMemo(() => mapCsvToObj(mapCSV), []);
  const mapMetaStore = useMemo(
    () => mapMetaToStoreCsvToObj(mapMetaToStore),
    []
  );
  const [sampleIndex, setSampleIndex] = useState(0);
  const state = useMapState(
    "modelLoaded",
    "setCameraPhi",
    "waitForRender",
    "setMetadata"
  );

  const handleClick = (direction) => {
    console.log("on submit event");
    let newIndex = 0;
    if (direction === "up") newIndex = sampleIndex + 1;
    if (direction === "down") newIndex = sampleIndex - 1;
    setSampleIndex(newIndex); //!
    const metadata = sampleMeta[newIndex]; // !testing mode

    // todo: rename function metaDataToObj
    const metaAttr = metaDataCsvToObj(metadata, mapMetaStore);
    console.log("added metadata");
    const haikuNodes = getHaikuNodes(mapHaikuCsv, metaAttr.haiku);

    const tokenIdArr = tokenIdDigits(metaAttr.token_id);
    const isGolden = metaAttr.golden === "false" ? false : true;
    state.setMetadata({
      metaData: metaAttr,
      mapMeta,
      exceptions,
      haikuNodes,
      tokenIdArr,
      isGolden,
    });
  };

  return (
    <>
      <button
        onClick={() => handleClick("up")}
        type="submit"
        id="update-metadata"
        className="absolute top-3 left-3 rounded-3xl bg-black text-white px-4 py-1"
      >
        Update Meta +
      </button>
      <button
        onClick={() => handleClick("down")}
        type="submit"
        id="update-metadata"
        className="absolute top-12 left-3 rounded-3xl bg-black text-white px-4 py-1"
      >
        Update Meta -
      </button>
    </>
  );
}
