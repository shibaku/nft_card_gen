import { useMapStore } from "store/useMapStore";
import parse from "html-react-parser";
import useAfterPreloader from "./controllers/useAfterPreloader";

export default function Preloader({ scene }) {
  const { preloader } = useMapStore(["preloader"]);
  const isActive = preloader?.active || false;
  //todo: can this be moved somewhere else?
  useAfterPreloader(isActive, scene?.afterPreloader);

  if (!isActive) return <></>;
  return <PreloaderComponent preloader={preloader} />;
}

function PreloaderComponent({ preloader }) {
  return (
    <div>
      <div
        className="absolute top-0 left-0 w-screen h-screen z-20 bg-center bg-no-repeat bg-cover"
        style={{
          backgroundImage: `url(${preloader.backgroundImg})`,
          boxShadow: " 0 0 400px 20px rgba(0,0,0,0.9) inset ",
        }}
      ></div>
      <div className="text-center absolute w-screen top-0 left-0 bg-black bg-opacity-70 h-screen z-40 text-white flex justify-center items-center">
        <div className="text-center">
          {preloader.img && (
            <img
              src={preloader.img}
              alt="preloader"
              className="mx-auto w-10/12 md:w-full"
            />
          )}
          {preloader.text && (
            <div className="text-4xl mt-2 mb-8 leading-normal filter drop-shadow-lg">
              {parse(preloader.text)}
            </div>
          )}
          <p className="text-2xl md:text-3xl mt-3 bg-white bg-opacity-20 w-max mx-auto px-7 py-2 rounded-3xl">
            Loading : {Math.floor(preloader.progress)}%
          </p>
        </div>
      </div>
    </div>
  );
}
