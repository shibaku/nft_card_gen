import { useMemo } from "react";
import { Canvas } from "@react-three/fiber";
// import Hls from "hls.js";
// import dashjs from "dashjs";

export default function TestStream3D() {
  const video = useMemo(() => {
    const vidEl = document.createElement("video");
    vidEl.autoPlay = true;
    vidEl.loop = true;
    vidEl.muted = true;
    vidEl.playsInline = true;
    vidEl.crossOrigin = "anonymous";

    let player;
    // const isIOS = true;
    // if (dashjs.supportsMediaSource()) {
    //   if (Hls.isSupported()) {
    //     const config = {
    //       manifestLoadingTimeOut: 20000,
    //       levelLoadingTimeOut: 20000,
    //       fragLoadingTimeOut: 20000,
    //       nudgeOffset: 0,
    //       nudgeMaxRetry: 10,
    //       loop: true,
    //     };
    //     player = new Hls(config);
    //     player.attachMedia(vidEl);
    //     player.on(Hls.Events.MEDIA_ATTACHED, function () {
    //       player.loadSource(
    //         "https://videodelivery.net/e77730d30974f44f015c8c8b0e4f8733/manifest/video.m3u8"
    //       );
    //     });
    //     player.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
    //       vidEl.play();
    //     });
    //   } else if (vidEl.canPlayType("application/vnd.apple.mpegurl")) {
    vidEl.src =
      // "https://d8d913s460fub.cloudfront.net/videoserver/cat-test-video-320x240.mp4";
      "https://videodelivery.net/e77730d30974f44f015c8c8b0e4f8733/manifest/video.m3u8";

    vidEl.play();
    // }
    // }

    return { vidEl, player };
  }, []);

  return (
    <div className="w-screen h-screen">
      <Canvas>
        <ambientLight camera={{ position: [0, 5, 5] }} />
        <mesh>
          <planeGeometry args={[3, 3]} />
          <meshBasicMaterial>
            <videoTexture attach="map" args={[video.vidEl]} />
          </meshBasicMaterial>
        </mesh>
      </Canvas>
    </div>
  );
}
