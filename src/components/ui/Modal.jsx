import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { useMapStore } from "store/useMapStore";
import parse from "html-react-parser";

export default function Modal() {
  const {
    modal: { active, title, body },
    toggleModal,
  } = useMapStore(["modal", "toggleModal"], "app");

  // let [isOpen, setIsOpen] = useState(true);
  // function closeModal() {
  //   setIsOpen(false);
  // }

  // function openModal() {
  //   setIsOpen(true);
  // }

  return (
    <>
      <Transition appear show={active} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          onClose={toggleModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div
                className="inline-block w-full max-w-[20rem] md:max-w-[27rem] py-6 px-2 my-8
              overflow-hidden text-left align-middle transition-all transform
               shadow-xl rounded-3xl bg-white bg-opacity-10 backdrop-filter backdrop-blur-xl text-white"
              >
                <Dialog.Title as="h3">{title && parse(title)}</Dialog.Title>
                {body && parse(body)}
                <div className="mt-4 text-center">
                  <button
                    type="button"
                    className="inline-flex justify-center px-10 py-2 text-sm md:text-base font-medium text-white bg-white bg-opacity-20 border-2 border-transparent rounded-3xl hover:bg-opacity-40 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-300 shadow-xl"
                    onClick={toggleModal}
                  >
                    Close
                  </button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
