import Button from "./Button";
import Modal from "./Modal";

export default function Ui({ ui }) {
  // !wrap into use callback
  const uiComponents = () => {
    // iterate through all ui groups
    const allUiElements = Object.keys(ui).map((group) => {
      const uiGroup = ui[group];

      const elements = uiGroup.elements;
      // all children in group
      const children = Object.entries(elements).map(([key, props], index) => {
        // ! add component selection based on element type
        // !  if (props.type === "button") then.....
        if (props?.hidden) return null;
        return (
          <Button key={key} elKey={key} index={index} parent={group} {...props}>
            {key}
          </Button>
        );
      });

      // position of group
      let pos;
      switch (uiGroup.position) {
        case "topLeft":
          pos = "top-0 left-0 xl:top-3 xl:left-3";
          break;
        case "bottomCenter":
          pos = "bottom-1 flex justify-center w-full";
          break;
        default:
          pos = "top-3 left-3";
      }

      // styling of group
      let style;
      if (uiGroup.frame)
        style = `border-2 p-2  rounded-br-xl rounded-tr-xl xl:rounded-tl-xl xl:rounded-bl-xl  bg-white bg-opacity-10 backdrop-filter backdrop-blur-xl`;

      //all groups with children
      return (
        <div className={`absolute ${pos}`} key={group}>
          <div className={`${style}`}>
            <div className="text-white text-lg mb-2 text-center select-none ">
              {uiGroup?.groupName && uiGroup.name}
            </div>
            <div>{children}</div>
          </div>
        </div>
      );
    });

    return allUiElements;
  };

  return (
    <>
      <Modal />
      {uiComponents()}
    </>
  );
}
