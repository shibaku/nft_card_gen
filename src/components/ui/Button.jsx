import { useMapStore } from "store/useMapStore";
import produce from "immer";

export default function Button(props) {
  const { setControllers } = useMapStore(["setControllers"], "app");
  // const bg = props.playing ? "bg-green-600" : "bg-white bg-opacity-30";

  // console.log(props.actions);
  const actionsPath = `inputs.ui.${props.parent}.elements.${props.elKey}.actions`;

  const onClick = (e) => {
    setControllers(props.actions, actionsPath);
    e.target.blur();
  };

  const onMouseUp = (e) => {
    if (!(props.onMouseUp === "revert")) return;
    // setTimeout(() => {
    const revertState = produce(props.actions, (draft) => {
      draft.forEach((action) => {
        action.state.active = false;
      });
    });
    setControllers(revertState, actionsPath);
    // }, 100);
  };

  let format;
  let visibility;
  if (props.large)
    format =
      "text-lg md:text-3xl 2xl:text-4xl px-7 py-2 pt-1 bg-white bg-opacity-30 backdrop-filter backdrop-blur-xl border-2 border-white border-opacity-70 shadow-2xl hover:bg-white hover:bg-opacity-30 hover:border-black hover:border-opacity-80 hover:shadow-lg";
  else if (props.round)
    format =
      "text-xs md:text-base md:text-md p-4 rounded-full bg-white bg-opacity-30 background focus:outline-none select-none";
  else
    format =
      "text-xs md:text-base md:text-md py-1 px-4 bg-white bg-opacity-30 background ";

  if (props.mobileOnly) visibility = "inline xl:hidden";

  return (
    <button
      onMouseDown={onClick}
      onMouseUp={onMouseUp}
      onTouchStart={onClick}
      onTouchEnd={onClick}
      className={`${format} ${visibility} text-left rounded-3xl block mb-3 w-full hover:bg-opacity-60 text-white select-none`}
    >
      {props.name}
      {props?.icon && (
        <img src={props.icon} className={`w-8 rounded-full`} alt="icon" />
      )}
    </button>
  );
}
