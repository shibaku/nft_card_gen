import { useEffect, useCallback, useRef } from "react";
import { useMapStore } from "store/useMapStore";
import { useUiStore } from "../../store/useUiStore";

export default function useUi(props) {
  const addedItems = useRef(false);
  const key = Object.keys(props)[0];
  const { setNewEl } = useMapStore(["setNewEl"], "ui");
  const store = useUiStore(
    useCallback((state) => state.uiElements[key], [key])
  );
  useEffect(() => {
    if (!Object.values(props)[0]) return;
    if (addedItems.current) return;
    setNewEl(key, props[key]);
    addedItems.current = true;
  }, [props, key, setNewEl]);
  return store;
}
