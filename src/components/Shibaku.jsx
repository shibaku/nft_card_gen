import { OrbitControls, useGLTF, useAnimations } from "@react-three/drei";
import { Canvas, useThree } from "@react-three/fiber";
import Loader from "./models/Loader";
import { useRef, useMemo, Suspense, useEffect } from "react";
import { DefaultHdri } from "./Hdri";

export default function Shibaku() {
  const ref = useRef();
  return (
    <div className="h-screen w-screen z-10 overflow-hidden bg-black">
      <Canvas
        ref={ref}
        camera={{
          position: [0, 0.04, 0.13],
          // !ybot
          // position: [0, 1, 2],
          near: 0.01,
          far: 2000,
          fov: 70,
        }}
        // frameloop="demand"
      >
        <Scene />
      </Canvas>
    </div>
  );
}

function Scene() {
  useToneMapping(0.64);
  useUpdateCamPosition(0, 0.04, 0.13);
  return (
    <>
      <pointLight intensity={0.4} position={[4, 4, 4]} />
      <Suspense fallback={<Loader image={"/images/shibaku.jpg"} />}>
        <Avatar />
        <DefaultHdri />
      </Suspense>
      <OrbitControls
        target={[0, 0.01, 0]}
        enableZoom={false}
        enablePan={false}
        maxPolarAngle={Math.PI / 2.1}
        minPolarAngle={Math.PI / 2.8}
      />
    </>
  );
}

function Avatar() {
  const primitive = useRef();
  const { scene, animations } = useGLTF(
    "/shibakuAvatar/shibakuAvatar.gltf",
    "/draco/"
  );
  const { actions, clips } = useAnimations(animations, primitive);
  useEffect(() => {
    console.log(clips);
    clips[0].duration = 1;
    clips[0].trim();
    actions.armitureAction.play();
    Object.keys(actions).map((action) => actions.action?.play());
    // actions.walking.play();
    // actions.jump.play();
  });

  return <primitive object={scene} position={[0, -0.091, 0]} ref={primitive} />;
}

function useToneMapping(value) {
  const { gl } = useThree();
  useMemo(() => (gl.toneMappingExposure = value), [gl, value]);
}

function useUpdateCamPosition(x, y, z) {
  const { camera } = useThree();
  useEffect(() => {
    camera.position.set(x, y, z);
    camera.updateProjectionMatrix();
  });
}
