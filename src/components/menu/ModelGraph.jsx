import { useState, useEffect, useCallback } from "react";
import { useMapState } from "hooks/useMapState";
import EyeOff from "../icons/EyeOff";
import Eye from "../icons/Eye";

export default function ModelGraph({ model, index }) {
  const rootNode = model.graph;
  return (
    <div className="ml-4">
      <SceneNode node={rootNode} modelIndex={index} childrensVis={false} />
    </div>
  );
}

function SceneNode({ node, modelIndex, childrensVis }) {
  // ? idea: set show to true based on nr. of children
  // ?to set default view based on hierarchy level
  const [showChildren, setShowChildren] = useState(false);
  const [nodeVisible, setNodeVisible] = useState(node.visible);
  const [allChildrenVis, setAllChildrenVis] = useState(false);
  const store = useMapState("setNodeVisibility", "setUpdateNodeAttributes");

  const toggleNodeVisibility = useCallback(
    (e) => {
      setNodeVisible(!nodeVisible);
      store.setNodeVisibility(modelIndex, node.graphIndex, !node.visible);
      // !combine handle action below inside of setNodeVisi.. inside of store
      store.setUpdateNodeAttributes(modelIndex, node.graphIndex, !node.visible);
    },
    [modelIndex, nodeVisible, node.graphIndex, node.visible, store]
  );

  // ! creates issues with reading nodeVis state from store, on click vis state of note changes
  // !automatically
  useEffect(() => {
    // if (childNodesVis !== nodeVisible) toggleNodeVisibility();
    if (childrensVis !== allChildrenVis) {
      toggleNodeVisibility();
      setAllChildrenVis(childrensVis);
    }
    //eslint-disable-next-line
  }, [childrensVis]);

  const children = (node.children || []).map((child) => {
    return (
      <SceneNode
        node={child}
        key={child.name}
        modelIndex={modelIndex}
        childrensVis={allChildrenVis}
      />
    );
  });

  function toggleChildren(e) {
    if (e.shiftKey) {
      setAllChildrenVis(!allChildrenVis);
    } else setShowChildren(!showChildren);
  }

  return (
    <div className="text-sm py-[0.1rem]">
      <div className="flex">
        <button onClick={toggleNodeVisibility}>
          {nodeVisible ? <Eye /> : <EyeOff />}
        </button>
        <p
          className="ml-1 hover:cursor-pointer select-none"
          onClick={toggleChildren}
        >
          {node.name}
        </p>
      </div>
      {showChildren && (
        <div className="ml-2 border-l-2 pl-[0.3rem]">{children}</div>
      )}
    </div>
  );
}
