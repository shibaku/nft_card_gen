import { useEffect } from "react";
import { useDropzone } from "react-dropzone";
import { useMapState } from "hooks/useMapState";

export default function UploadHdriButton({ closeModal }) {
  const state = useMapState("setHdri");

  const { getRootProps, getInputProps, acceptedFiles } = useDropzone({});

  // need to wrap into use effect to avoid paralell render warning
  useEffect(() => {
    acceptedFiles.map((file) => {
      const url = URL.createObjectURL(acceptedFiles[0]);
      state.setHdri(url);
      return null;
    });
  });

  return (
    <div className="text-center mt-1 w-full">
      <button
        {...getRootProps()}
        className="w-full text-center text-md inline-flex justify-center focus:outline-none bg-gradient-to-r from-purple-700 to-blue-700 px-5 py-1 rounded-xl text-white"
        autoFocus
      >
        Upload Hdri
        {/* </button> */}
        <input {...getInputProps()} className="block" />
      </button>
    </div>
  );
}
