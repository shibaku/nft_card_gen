// import { useEffect } from "react";
import { useMapState } from "hooks/useMapState";
import ModelsAndLayers from "./ModelsAndLayers";
import UploadHdriButton from "./UploadHdriButton";
import GltfUploadDialog from "./GltfUploadDialog";

export default function Menu() {
  return (
    <div className="absolute top-3 left-2 z-50 w-34">
      <ModelsAndLayers />
      <UploadGltfButton />
      <UploadHdriButton />
      <GltfUploadDialog />
    </div>
  );
}

function UploadGltfButton() {
  const store = useMapState("setUploadDialog", "setVisible");

  function onClick() {
    store.setUploadDialog(true);
  }

  return (
    <button
      onClick={onClick}
      className="mt-1 w-full text-center inline-flex justify-center focus:outline-none bg-gradient-to-r from-purple-700 to-blue-700 px-5 py-1 rounded-xl text-white"
    >
      Upload GLTF
    </button>
  );
}
