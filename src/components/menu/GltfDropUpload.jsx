import { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { useMapState } from "hooks/useMapState";
import createFileMap from "utils/createFileMap";

export default function GltfDropUpload({ closeModal }) {
  const state = useMapState("setFileMaps");

  const onDrop = useCallback(
    (acceptedFiles) => {
      closeModal();
      const { name, rootFile, map } = createFileMap(acceptedFiles);
      state.setFileMaps(name, rootFile, map);
    },
    [state, closeModal]
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
  });

  const placeholder = isDragActive ? (
    <p className="text-md text-gray-500 focus:border-none focus:outline-none ring-transparent focus:ring-transparent outline-none ">
      <br />
      ...drop <br />
      files... <br />
    </p>
  ) : (
    <p className="text-md text-gray-500 focus:border-none focus:outline-none ring-transparent focus:ring-transparent outline-none ">
      Drag GLtf and drop <br /> GLTF folder here <br /> or click upload button
    </p>
  );

  return (
    <div
      {...getRootProps({ className: "dropzone" })}
      className="mt-6 text-center border-2 border-dashed w-7/12 py-28 mx-auto border-blue-300 "
    >
      <input
        {...getInputProps()}
        className="block w-full h-full"
        webkitdirectory=""
      />
      {placeholder}
    </div>
  );
}
