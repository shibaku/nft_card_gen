import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";
import GltfDropUpload from "./GltfDropUpload";
import UploadGltfButton from "./UploadGltfButton";
import { useMapState } from "hooks/useMapState";

export default function GltfUploadDialog() {
  const { uploadDialog, setUploadDialog } = useMapState(
    "uploadDialog",
    "setUploadDialog"
  );

  function closeModal() {
    setUploadDialog(false);
  }

  // function openModal() {
  //   setUploadDialog(true);
  // }

  return (
    <>
      <Transition appear show={uploadDialog} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div
                className="inline-block w-full max-w-md p-6 my-8 
              overflow-hidden text-left align-middle transition-all transform 
               shadow-xl rounded-2xl bg-white bg-opacity-10 backdrop-filter backdrop-blur-3xl"
              >
                <Dialog.Title
                  as="h3"
                  className="text-2xl text-center  leading-6 text-black"
                >
                  Upload GLTF
                </Dialog.Title>
                <GltfDropUpload closeModal={closeModal} />
                <div className="mt-6 text-center">
                  <UploadGltfButton closeModal={closeModal} />
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
