import { useState } from "react";
import { useMapState } from "hooks/useMapState";
import ModelGraph from "./ModelGraph";

export default function ModelsAndLayers() {
  const store = useMapState("modelGraphs", "updateVisibility");
  const [showModels, setShowModels] = useState(true);

  const modelList = store.modelGraphs.map((model, index) => {
    // ? ?not sure if still need to toggle visibility of gltf primitive
    // is this needed going forward?
    // const backgroundColor = model?.visible
    //   ? "bg-green-500 bg-opacity-60"
    //   : "bg-gray-50 bg-opacity-20";

    const backgroundColor = "bg-gray-50 bg-opacity-20";

    return (
      <div key={index}>
        <button
          className={` ${backgroundColor} mb-2 mt-2 rounded-lg pl-4 pr-2 text-white focus:outline-none block w-full text-left`}
          key={index}
          // ?not sure if still need to toggle visibility of gltf primitive
          // onClick={() => store.updateVisibility(index)}
        >
          {model.name}
        </button>
        <ModelGraph model={model} index={index} />
      </div>
    );
  });

  return (
    <div className="bg-gradient-to-r  from-purple-700 to-blue-700 px-3 py-3 rounded-2xl text-white min-w-[17rem] ">
      <button
        className="pl-2 my-1 w-full  text-xl text-center bg-gray-50 bg-opacity-25 rounded-lg focus:outline-none"
        onClick={() => setShowModels(!showModels)}
      >
        Models
      </button>
      {showModels && (
        <div className=" max-h-[33rem]  overflow-auto">{modelList}</div>
      )}
    </div>
  );
}
