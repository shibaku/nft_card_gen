import { useControls } from "leva";
import { BackSide, TextureLoader } from "three";
import { useLoader } from "@react-three/fiber";

export default function SkyDome({
  emission = 1,
  args = [20, 32, 16],
  ...props
}) {
  const { skyDome, skyDomeEmission } = useControls({
    skyDome: true,
    skyDomeEmission: {
      value: emission,
      min: 0,
      max: 4,
      step: 0.05,
    },
  });
  const { skyDomeParam } = useControls({
    skyDomeParam: { radius: args[0], widthSeg: args[1], heightSeg: args[2] },
  });
  const background = useLoader(TextureLoader, "/sky.jpg");
  const lightMap = useLoader(TextureLoader, "/lightmap.jpg");

  // !todo: change side from double to inside

  return (
    <mesh visible={skyDome}>
      <sphereGeometry
        args={[
          skyDomeParam.radius,
          skyDomeParam.widthSeg,
          skyDomeParam.heightSeg,
        ]}
      />
      <meshBasicMaterial
        side={BackSide}
        map={background}
        lightMap={lightMap}
        lightMapIntensity={skyDomeEmission}
      />
    </mesh>
  );
}
