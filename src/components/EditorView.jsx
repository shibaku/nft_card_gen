import Menu from "./menu/Menu";
import { Suspense } from "react";
import { Canvas } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";
import Hdri from "./Hdri";
import ModelList from "./models/ModelList";
import { useControls } from "leva";
import CameraSettings from "./CameraSettingsEditor";
import SkyDome from "./SkyDome";
import PostprocessingCustom from "./PostprocessingCustom";
// import Postprocessing from "./Postprocessing";

export default function EditorView() {
  const { pointLight, ambientLight, lookAtY } = useControls({
    pointLight: 2.19,
    ambientLight: 1.41,
    lookAtY: 0.05,
  });

  return (
    <div className="w-screen h-screen bg-black">
      <Menu />
      <div className="w-full h-full">
        <Canvas frameloop="demand" gl={{ antialias: true }}>
          <CameraSettings />
          <ambientLight intensity={ambientLight} />
          <pointLight intensity={pointLight} position={[0, 10, 4]} />
          <Suspense fallback={null}>
            <SkyDome />
          </Suspense>
          <Suspense fallback={null}>
            <Hdri />
          </Suspense>
          <Suspense fallback={null}>
            <ModelList />
          </Suspense>
          <OrbitControls target={[0, lookAtY, 0]} />
          {/* <Postprocessing /> */}
          <PostprocessingCustom />
        </Canvas>
      </div>
    </div>
  );
}
