import { useGLTF } from "@react-three/drei";

export default function Head() {
  // const ref = useRef();
  const gltf = useGLTF("/headcomp/head.gltf", "/draco/");
  return <primitive object={gltf.scene} position={[0, -0.091, 0]} />;
}
