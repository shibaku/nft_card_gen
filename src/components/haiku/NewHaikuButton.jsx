import { useEffect } from "react";
import { useSpeechSynthesis } from "react-speech-kit";
import { useMapStore } from "store/useMapStore";
import haikus from "data/haikus.json";

function randomHaikuArr() {
  return haikus.map((line) =>
    line[Math.floor(Math.random() * line.length)].toUpperCase()
  );
}

export default function NewHaikuButton() {
  const store = useMapStore(["setHaikuArr"], "haiku");
  const { speak } = useSpeechSynthesis();

  useEffect(() => {
    const randomLines = randomHaikuArr();
    store.setHaikuArr(randomLines);
    // eslint-disable-next-line
  }, []);

  function newHaiku() {
    const randomLines = randomHaikuArr();
    const fullHaiku = randomLines.reduce(
      (prevValue, currentValue) => prevValue + "," + currentValue
    );
    speak({
      text: fullHaiku,
    });
    store.setHaikuArr(randomLines);
  }

  return (
    <div className="absolute bottom-5  w-full text-center">
      <button
        onClick={newHaiku}
        className="inline-block animate-pulse  border-solid border-[1px] border-opacity-50 border-white rounded-3xl text-xl lg:text-2xl bg-white  bg-opacity-30 text-blue-100 backdrop-filter backdrop-blur-lg active:outline-none"
      >
        <div className="select-none bg-gradient-to-b from-[rgba(255,0,0,0)] to-[rgba(68,0,255,0.3)] w-full h-full px-5 py-3 overflow-hidden rounded-3xl">
          Bestow New Dog Wisdom
        </div>
      </button>
    </div>
  );
}
