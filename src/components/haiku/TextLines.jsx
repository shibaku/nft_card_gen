import { useEffect, useState } from "react";
import Text from "./Text";
import { Suspense } from "react";
import { useMapStore } from "store/useMapStore";

function typeText(textArr, textIncr, index, maxIndex, setState) {
  const diff = textArr[index].length - textIncr[index].length;
  if (diff >= 1) {
    setTimeout(() => {
      const newLine = textIncr[index] + textArr[index][textIncr[index].length];
      const newArray = [...textIncr];
      newArray[index] = newLine;
      setState(newArray);
    }, 50);
  } else {
    if (index + 1 >= maxIndex) return;
    const newIndex = index + 1;
    typeText(textArr, textIncr, newIndex, maxIndex, setState);
  }
}

function TextLines({ scale, lineHeight }) {
  const { haikuArr: textArr } = useMapStore(["haikuArr"], "haiku");
  const [textIncr, setTextIncr] = useState(Array(3).fill(""));

  useEffect(() => {
    setTextIncr(Array(3).fill(""));
  }, [textArr]);

  useEffect(() => {
    typeText(textArr, textIncr, 0, 3, setTextIncr);
    // eslint-disable-next-line
  }, [textIncr]);

  let lineHeightIncrement = 0;
  const textLines = textIncr.map((haikuLine, index) => {
    lineHeightIncrement += lineHeight;
    return (
      <Text
        children={haikuLine}
        scale={scale}
        position={[0, lineHeightIncrement, 0]}
        key={index}
      />
    );
  });

  return (
    <Suspense fallback={null}>
      <group position={[0, 0.003, 0.009]}>{textLines}</group>
    </Suspense>
  );
}

export default TextLines;
