import { useMemo, Suspense } from "react";
import { OrbitControls } from "@react-three/drei";
import { useThree } from "@react-three/fiber";
import { DefaultHdri } from "../Hdri";
import TextLines from "./TextLines";
import Head from "./Head";
import Stage from "./Stage";
import Loader from "../models/Loader";
import useResponsiveZoom from "hooks/useResponsiveZoom";

export default function HaikuScene() {
  const scale = useMemo(() => 0.00022, []);
  useToneMapping(0.5);
  useResponsiveZoom(0, 1020, [0, 0.016, 0.25]);

  return (
    <>
      <ambientLight intensity={0.2} color={"white"} />
      <Suspense fallback={null}>
        <DefaultHdri />
      </Suspense>
      <TextLines scale={scale} lineHeight={-0.01} />
      <Suspense fallback={<Loader image={"/images/shiba-haiku.jpg"} />}>
        <Stage />
        <Head />
      </Suspense>
      <mesh position={[0, -0.02, 0.006]} rotation={[0, 0, 0]}>
        <planeGeometry args={[0.14, 0.05]} />
        <meshPhysicalMaterial
          roughness={0.5}
          metalness={1}
          color={0xded1d2}
          specularIntensity={1}
        />
      </mesh>
      <OrbitControls
        enableZoom={false}
        enablePan={false}
        maxAzimuthAngle={Math.PI / 6.2}
        minAzimuthAngle={-Math.PI / 6.2}
        maxPolarAngle={Math.PI / 2.1}
        minPolarAngle={Math.PI / 2.8}
      />
    </>
  );
}

function useToneMapping(value) {
  const { gl } = useThree();
  useMemo(() => (gl.toneMappingExposure = value), [gl, value]);
}
