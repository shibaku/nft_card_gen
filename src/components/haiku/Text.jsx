import { useRef, useLayoutEffect, useMemo } from "react";
import { useLoader } from "@react-three/fiber";
import { FontLoader, Vector3 } from "three";
import { DefaultHdri } from "../Hdri";

export default function Text({ children, scale, ...props }) {
  const font = useLoader(FontLoader, "./roboto.json");
  const mesh = useRef();
  //
  useLayoutEffect(() => {
    const size = new Vector3();
    mesh.current.geometry.computeBoundingBox();
    mesh.current.geometry.boundingBox.getSize(size);
    mesh.current.position.x = -size.x / 2;
    mesh.current.position.y = -size.y / 2;
  }, [children]);

  const config = useMemo(
    () => ({
      font,
      size: 25,
      height: 5,
      curveSegments: 10,
      bevelEnabled: true,
      bevelThickness: 1,
      bevelSize: 1,
      // bevelOffset: 0,
      // bevelSegments: 8,
    }),
    [font]
  );
  return (
    <group scale={[scale, scale, scale]} {...props}>
      <mesh ref={mesh}>
        <textGeometry args={[children, config]} />
        {/* <meshBasicMaterial color={"white"} /> */}
        <meshStandardMaterial color={"purple"} />
        {/* <meshNormalMaterial /> */}
      </mesh>
      <DefaultHdri />
    </group>
  );
}
