import { useMemo } from "react";
import { useThree } from "@react-three/fiber";
import { useControls } from "leva";

export default function ToneMappingController({
  controls = false,
  scene,
  ...props
}) {
  const exposure = scene.render.toneMappingExposure;
  if (controls) return <WithControls toneMapExp={exposure} {...props} />;
  return <WithProps toneMapExp={exposure} {...props} />;
}
function WithProps({ toneMapExp }) {
  const { gl } = useThree();
  useMemo(() => (gl.toneMappingExposure = toneMapExp), [gl, toneMapExp]);
  return <></>;
}

function WithControls({ toneMapExp, ...props }) {
  const { exposure } = useControls({
    exposure: {
      value: toneMapExp,
      min: 0,
      max: 3,
      step: 0.01,
    },
  });
  const { gl } = useThree();
  useMemo(() => (gl.toneMappingExposure = exposure), [gl, exposure]);
  return <></>;
}
