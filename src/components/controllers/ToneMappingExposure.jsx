import { useMemo } from "react";
import { useThree } from "@react-three/fiber";
import { useControls } from "leva";

export default function ToneMappingExposure({ controls = false, ...props }) {
  if (controls) return <WithControls />;
  return <WithProps {...props} />;
}

function WithControls() {
  const { exposure } = useControls({
    exposure: {
      value: 0.45,
      min: 0,
      max: 1,
      step: 0.01,
    },
  });
  const { gl } = useThree();
  useMemo(() => (gl.toneMappingExposure = exposure), [gl, exposure]);
  return <></>;
}

function WithProps({ exposure }) {
  const { gl } = useThree();
  useMemo(() => (gl.toneMappingExposure = exposure), [gl, exposure]);
  return <></>;
}
