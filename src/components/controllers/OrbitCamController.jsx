import { OrbitControls } from "@react-three/drei";
import { useThree } from "@react-three/fiber";
import { useEffect, useRef } from "react";
import { useControls } from "leva";
import useTargetOrbitTween from "./useTargetOrbitTween";

export default function OrbitCamController({
  camera,
  controllers,
  device,
  makeDefault,
}) {
  const orbitControls = controllers.orbitControls;
  //todo: can the WithControls be moved into separate, independent component?
  if (camera.uiControls)
    return (
      <WithControls
        camera={camera}
        orbitControls={orbitControls}
        device={device}
        makeDefault={makeDefault}
      />
    );
  return (
    <WithProps
      camera={camera}
      orbitControls={orbitControls}
      device={device}
      makeDefault={makeDefault}
    />
  );
}

function WithProps({ orbitControls, device, makeDefault }) {
  const orbitRef = useRef();
  const {
    enableZoom,
    enablePan,
    maxAzimuthAngle,
    minAzimuthAngle,
    maxPolarAngle,
    minPolarAngle,
    target,
  } = orbitControls;

  useTargetOrbitTween(orbitRef, target[device], device);

  return (
    <>
      <OrbitControls
        ref={orbitRef}
        enableZoom={enableZoom}
        enablePan={enablePan}
        maxAzimuthAngle={maxAzimuthAngle}
        minAzimuthAngle={minAzimuthAngle}
        maxPolarAngle={maxPolarAngle}
        minPolarAngle={minPolarAngle}
        target={target[device]}
        screenSpacePanning={false}
        makeDefault={makeDefault}
      />
    </>
  );
}

function WithControls({
  camera: cam,
  orbitControls,
  device,
  makeDefault,
  ...props
}) {
  const { fov, near, far, position } = cam;
  const { target } = orbitControls;
  const { camPosition, targetControl, camFrustum } = useControls({
    camPosition: {
      x: position[device][0],
      y: position[device][1],
      z: position[device][2],
    },
    targetControl: {
      x: target[device][0],
      y: target[device][1],
      z: target[device][2],
    },
    camFrustum: { fov, near, far },
  });

  const { camera, gl } = useThree();
  useEffect(() => {
    camera.fov = camFrustum.fov;
    camera.near = camFrustum.near;
    camera.far = camFrustum.far;
    camera.position.x = camPosition.x;
    camera.position.y = camPosition.y;
    camera.position.z = camPosition.z;
    camera.updateProjectionMatrix();
  }, [camFrustum, camera, gl, camPosition]);

  return (
    <OrbitControls
      target={Object.values(targetControl)}
      makeDefault={makeDefault}
    />
  );
}
