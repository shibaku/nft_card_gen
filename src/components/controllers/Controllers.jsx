import TransformController from "components/controllers/TransformController";
import ToneMappingController from "../controllers/ToneMappingController";
import useCameraTween from "./useCameraTween";
import OrbitCamController from "./OrbitCamController";
import FpsController from "./FpsController";

export default function Controllers({ scene, device }) {
  const { transformController, fpsControls, orbitControls } = scene.controllers;

  useCameraTween(device);

  return (
    <>
      <ToneMappingController scene={scene} />
      {orbitControls?.active && (
        <OrbitController scene={scene} device={device} />
      )}
      {transformController?.active && (
        <TransformController config={transformController.config} />
      )}
      {fpsControls?.active && <FpsController />}
    </>
  );
}

function OrbitController({ scene, device }) {
  return (
    <OrbitCamController
      camera={scene.camera}
      controllers={scene.controllers}
      device={device}
      makeDefault={scene.controllers.orbitControls.makeDefault}
    />
  );
}
