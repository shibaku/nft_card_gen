import { useEffect } from "react";
import { useMapStore } from "store/useMapStore";

export default function useAfterPreloader(isActive, afterPreloader) {
  const { setControllers } = useMapStore(["setControllers"], "app");

  useEffect(() => {
    if (isActive) return;
    setControllers(afterPreloader.actions);
  }, [isActive, afterPreloader, setControllers]);
}
