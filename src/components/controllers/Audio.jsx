import useSound from "use-sound";
import { useEffect, useState } from "react";
import { useMapStore } from "store/useMapStore";

export default function Audio({ audio }) {
  const audioComponents = Object.entries(audio).map(
    ([name, { config, state }]) => (
      <UseAudioComponent name={name} config={config} state={state} key={name} />
    )
  );
  return <>{audioComponents}</>;
}

function UseAudioComponent({ name, config, state }) {
  const { audio, setAudio } = useMapStore(["audio", "setAudio"], "app");
  const [playing, setPlaying] = useState(false);
  const [disable, setDisable] = useState(false);

  const [play, { stop }] = useSound(config.url, {
    ...config,
    onend: onEnd,
  });

  function onEnd() {
    if (config.playTillEnd) {
      setPlaying(false);
    }
  }

  useEffect(() => {
    if (name in audio && !playing && !disable) {
      play();
      if (config?.loop) setPlaying(true);
      else if (config.playTillEnd) {
        setPlaying(true);
        setAudio({ name });
      } else if (config?.useOnce) setDisable(true);
      else setAudio({ name }); //remove if no loop
    }
    if (!(name in audio) && playing && !config.playTillEnd) {
      stop();
      if (config.loop) setPlaying(false);
    }

    //if no loop remove from state here
    //stop if loop and update removed track from audio
    //track if track is playing with local storage for now
  }, [
    play,
    audio,
    name,
    playing,
    stop,
    config.loop,
    disable,
    config.useOnce,
    setAudio,
    config.playTillEnd,
  ]);

  return <></>;
}
