import { useThree } from "@react-three/fiber";
import { useSpring } from "@react-spring/three";
import { useMapStore } from "store/useMapStore";
import { useEffect } from "react";

export default function useCameraTween(device) {
  const { cameraTween, setCameraTween, setControllers } = useMapStore(
    ["cameraTween", "setCameraTween", "setControllers"],
    "app"
  );

  const tweenTo = cameraTween?.position[device];
  const duration = cameraTween.duration || 1000;

  const { camera } = useThree();
  const { x, y, z } = camera.position;

  // !abstract this into tween hook that can be used elsewhere
  // eslint-disable-next-line
  const [styles, tween] = useSpring(() => ({
    loop: false,
    position: [0, 0, 0],
    onChange: ({ value }) => {
      const pos = value.position;
      camera.position.set(pos[0], pos[1], pos[2]);
    },
  }));

  // ! only tween if values are different from previous tween
  useEffect(() => {
    if (!cameraTween.active) return;
    tween.start({
      config: { duration },
      from: {
        position: [x, y, z],
      },
      to: {
        position: tweenTo,
      },
      onRest: () => {
        tween.stop();
        if (cameraTween?.onComplete) {
          // console.log(cameraTween);
          const onCompletePath = `${cameraTween.path}.onComplete`;
          setControllers(cameraTween.onComplete, onCompletePath);
        }
        const updatedTween = { ...cameraTween };
        updatedTween.active = false;
        setCameraTween(updatedTween);
      },
    });
  }, [
    cameraTween.active,
    tween,
    tweenTo,
    x,
    y,
    z,
    duration,
    cameraTween,
    setCameraTween,
    setControllers,
  ]);
}
