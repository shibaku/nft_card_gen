import { useSpring } from "@react-spring/three";
import { useMapStore } from "store/useMapStore";
import { Vector3 } from "three";
import { useEffect } from "react";

export default function useTargeOrbitTween(orbitRef, initTarget, device) {
  const { cameraTargetTween, setCameraTargetTween, camera, setCamera } =
    useMapStore(
      ["cameraTargetTween", "setCameraTargetTween", "camera", "setCamera"],
      "app"
    );
  const currentTarget = camera.target || initTarget; //target is the default scene init value
  const newTarget = cameraTargetTween?.target[device];
  const duration = cameraTargetTween.duration || 1000;

  // !extract this into separate hook with refactored generall hook for tween
  //eslint-disable-next-line
  const [targetStyles, targetTween] = useSpring(() => ({
    loop: false,
    target: currentTarget,
    onChange: ({ value }) => {
      const target = value.target;
      orbitRef.current.target = new Vector3(...target);
    },
    onRest: ({ value }) => {
      targetTween.stop();
      const updatedTween = { ...cameraTargetTween };
      updatedTween.active = false;
      setCameraTargetTween(updatedTween);
      setCamera({ target: value.target });
    },
  }));

  useEffect(() => {
    if (!cameraTargetTween.active) return;
    targetTween.start({
      config: { duration },
      from: {
        target: currentTarget,
      },
      to: {
        target: newTarget,
      },
    });
  }, [
    cameraTargetTween.active,
    newTarget,
    targetTween,
    currentTarget,
    duration,
  ]);
}
