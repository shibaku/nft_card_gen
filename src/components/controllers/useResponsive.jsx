import { useMediaQuery } from "react-responsive";
// import { useEffect } from "react";

export default function useResponsive(position, responsive) {
  // const isTablet = useMediaQuery({
  //   query: "(min-width: 768px) and (max-width: 1024px)",
  // });
  const isDefault = useMediaQuery({
    query: "(min-width: 768px)",
  });

  if (isDefault) return "default";
  else return "mobile";
}
