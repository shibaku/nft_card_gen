import { Vector2 } from "three";
import { useEffect, useRef, useMemo } from "react";
import { extend, useThree, useFrame } from "@react-three/fiber";

import { EffectComposer } from "three-stdlib/postprocessing/EffectComposer";
import { RenderPass } from "three-stdlib/postprocessing/RenderPass";
import { UnrealBloomPass } from "three-stdlib/postprocessing/UnrealBloomPass";
import { ShaderPass } from "three-stdlib/postprocessing/ShaderPass";
import { SAOPass } from "three-stdlib/postprocessing/SAOPass";

import { FXAAShader } from "three-stdlib/shaders/FXAAShader";
import { VignetteShader } from "shaders/VignetteShader";
import { HueSaturationShader } from "shaders/hueSaturationShader";

import settings from "../data/renderSettigs.json";

const { hue, saturation, bloom, vignette, ambientOcclusion } = settings.effects;

extend({
  EffectComposer,
  RenderPass,
  UnrealBloomPass,
  ShaderPass,
  SAOPass,
});

export default function PostprocessingCustom() {
  const composer = useRef();
  const { scene, gl, size, camera } = useThree();

  const aspect = useMemo(() => new Vector2(size.width, size.height), [size]);
  const vignetteFunction = useMemo(
    () => VignetteShader(vignette.offset, vignette.darkness),
    []
  );
  const hueShader = useMemo(() => HueSaturationShader(hue, saturation), []);

  useEffect(() => composer.current.setSize(size.width, size.height), [size]);
  useFrame(() => {
    composer.current.render();
  }, 1);

  return (
    <effectComposer ref={composer} args={[gl]}>
      <renderPass attachArray="passes" scene={scene} camera={camera} />
      <unrealBloomPass
        attachArray="passes"
        args={[aspect, bloom.strength, bloom.treshold, bloom.radius]}
      />
      <sAOPass
        attachArray="passes"
        args={[scene, camera, false, true, { x: size.width, y: size.height }]}
        params={{
          output: ambientOcclusion.output,
          saoBias: ambientOcclusion.saoBias,
          saoIntensity: ambientOcclusion.saoIntensity,
          saoScale: ambientOcclusion.saoScale,
          saoKernelRadius: ambientOcclusion.saoKernelRadius,
          saoMinResolution: ambientOcclusion.saoMinResolution,
          saoBlur: ambientOcclusion.saoBlur,
          saoBlurRadius: ambientOcclusion.saoBlurRadius,
          saoBlurStdDev: ambientOcclusion.saoBlurStdDev,
          saoBlurDepthCutoff: ambientOcclusion.saoBlurDepthCutoff,
        }}
      />
      <shaderPass attachArray="passes" args={[hueShader]} />
      <shaderPass attachArray="passes" args={[vignetteFunction]} />
      <shaderPass
        attachArray="passes"
        args={[FXAAShader]}
        material-uniforms-resolution-value={[1 / size.width, 1 / size.height]}
        renderToScreen
      />
    </effectComposer>
  );
}
