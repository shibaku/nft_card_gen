import {
  useGLTF,
  Environment,
  // useAnimations,
} from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import { useRef, Suspense } from "react";
import Postprocessing from "./Postprocessing";
import CameraController from "./CameraController";
import SkyDome from "./SkyDome";
import ToneMappingExposure from "./controllers/ToneMappingExposure";
import Loader from "./models/Loader";

export default function Bridge() {
  const ref = useRef();
  return (
    <div className="h-screen w-screen z-10 overflow-hidden bg-black">
      <Canvas
        ref={ref}
        camera={{
          position: [13, 13, 4],
          near: 0.01,
          far: 2000,
          fov: 80,
        }}
        // frameloop="demand"
      >
        <Scene />
      </Canvas>
    </div>
  );
}

function Scene() {
  // useToneMapping(0.64);
  // useUpdateCamPosition(0, 0.04, 0.13);
  return (
    <>
      <pointLight intensity={1.4} position={[-20, 20, 20]} />
      <Suspense fallback={<Loader image={"/images/gallery.jpg"} />}>
        <BridgeComponent />
        <Environment preset={"park"} />
        <SkyDome emission={3.5} args={[180, 32, 16]} />
      </Suspense>
      <CameraController position={[-12.5, 11, 12.5]} lookAt={[0, 6, 0]} />
      <ToneMappingExposure exposure={0.43} />
      <Postprocessing
        intensity={1.5}
        threshold={0.5}
        smooth={0.1}
        controls={true}
      />
    </>
  );
}

function BridgeComponent() {
  const primitive = useRef();
  const { scene } = useGLTF("/bridge/bridge.gltf", "/draco/");

  return <primitive object={scene} position={[0, -0.091, 0]} ref={primitive} />;
}

// function useUpdateCamPosition(x, y, z) {
//   const { camera } = useThree();
//   useEffect(() => {
//     camera.position.set(x, y, z);
//     camera.updateProjectionMatrix();
//   });
// }
