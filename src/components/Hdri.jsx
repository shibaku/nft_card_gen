import { Environment } from "@react-three/drei";
import { useMapState } from "../hooks/useMapState";
import { useControls } from "leva";

export default function Hdri() {
  const { defaultHdri } = useControls({ defaultHdri: true });
  const { hdri } = useMapState("hdri");

  if (defaultHdri) return <Environment files="environment.hdr" />;
  else if (hdri && !defaultHdri) return <Environment files={hdri} />;
  else return <></>;
}

export function DefaultHdri() {
  return <Environment files="environment.hdr" />;
}

// to have background on hdri
// return <>{hdri && <Environment files={hdri} background />}</>;
