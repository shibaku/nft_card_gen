import { useThree, useFrame } from "@react-three/fiber";
import { useEffect } from "react";
import { useCreateSphereCoord } from "hooks/useCreateCoord";
import { useMapState } from "hooks/useMapState";
import settings from "../data/renderSettigs.json";

const deg = Math.PI / 180;

export default function CameraRenderControl() {
  const state = useMapState(
    "cameraPhi",
    "cameraPhiStepSize",
    "setWaitForRender"
  );
  const sphereCoord = useCreateSphereCoord();
  const { camera, gl, invalidate } = useThree();

  useFrame(() => {
    state.setWaitForRender(false);
  });

  useEffect(() => {
    //!set only once
    gl.toneMappingExposure = settings.exposure;

    if (!sphereCoord) return;
    // !set only once
    sphereCoord.setFromCartesianCoords(
      camera.position.x,
      camera.position.y,
      camera.position.z
    );

    const dTheta = deg * state.cameraPhi * settings.dPhiStepSize;
    sphereCoord.theta = dTheta;
    camera.position.setFromSpherical(sphereCoord);
    camera.lookAt(0, settings.cameraLookAtY, 0);
    camera.updateProjectionMatrix();
    invalidate();
  });

  return <></>;
}
