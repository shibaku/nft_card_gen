import { EffectComposer, Bloom } from "@react-three/postprocessing";
import { useControls } from "leva";
// import { KernelSize } from "postprocessing";

export default function Postprocessing({ controls = false, ...props }) {
  console.log(props);
  if (controls) return <WithControls />;
  return <WithProps {...props} />;
}

function WithProps(props) {
  console.log(props);
  return <Effects {...props} />;
}

function WithControls() {
  const controls = useControls({
    intensity: 1.5,
    threshold: 0.5,
    smooth: 0.1,
  });
  return <Effects {...controls} />;
}

function Effects({ intensity, threshold, smooth }) {
  return (
    <EffectComposer>
      <Bloom
        intensity={intensity}
        luminanceThreshold={threshold}
        luminanceSmoothing={smooth}
        // kernelSize={KernelSize.LARGE}
      />
    </EffectComposer>
  );
}
