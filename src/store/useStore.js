import create from "zustand";
import { devtools } from "zustand/middleware";
import produce from "immer";
import staticModels from "../data/staticModels.json";

export const useStore = create(
  devtools(
    (set, get) => ({
      preloader: null,
      setPreloader: (preloader) => set({ preloader }),

      // ####### Editor And Puppeteer Render ######
      uploadDialog: false,
      setUploadDialog: (state) => set({ uploadDialog: state }),

      //##Models
      staticModelPaths: staticModels,
      fileMaps: [],
      modelGraphs: [],
      updateNodeAttributes: [],
      // visible: true,

      setFileMaps: (name, rootFile, map) => {
        const newMap = {
          name,
          rootFile,
          map,
        };
        set((state) => ({ fileMaps: [...state.fileMaps, newMap] }));
      },
      addModelGraph: (index, name, visible, graph) => {
        set(
          produce((state) => {
            state.modelGraphs[index] = {
              name,
              visible,
              graph,
            };
          })
        );
      },
      setNodeVisibility: (modelIndex, graphIndex, visible) => {
        set(
          produce((state) => {
            const rootNode = state.modelGraphs[modelIndex].graph;
            //todo: refactor into function/or recursive function
            let node;
            graphIndex.forEach((el, index) => {
              if (index === 0) {
                node = rootNode;
              } else {
                node = node.children[el];
              }
            });
            node.visible = visible;
          })
        );
      },
      setUpdateNodeAttributes: (modelIndex, graphIndex, visible) =>
        set(
          produce((state) => {
            state.updateNodeAttributes.push({
              modelIndex,
              graphIndex,
              visible,
            });
          })
        ),
      // setUpdateNodeAttributes: (modelIndex, graphIndex, visible) =>
      //   set({ updateNodeAttributes: { modelIndex, graphIndex, visible } }),
      // !todo: check which one is not used anymore
      setVisible: () => set((state) => ({ visible: !state.visible })),
      updateVisibility: (index) =>
        set(
          produce((state) => {
            state.modelGraphs[index].visible =
              !state.modelGraphs[index].visible;
          })
        ),

      //##HDRI
      hdri: null,
      hdriFileMap: null,
      setHdri: (url) => set({ hdri: url }),
      setHdriMap: (map) => set({ hdriFileMap: map }),

      //##Puppetteer
      modelLoaded: false,
      cameraPhi: 0,
      cameraPhiStepSize: 1,
      waitForRender: false,
      metaData: false,
      setModelLoaded: () =>
        set((state) => ({ modelLoaded: !state.modelLoaded })),
      resetCameraPhi: () => set({ cameraPhi: 0 }),
      increaseCameraPhi: (phi) => {
        set((state) => ({
          cameraPhi: state.cameraPhi + 1 * state.cameraPhiStepSize,
        }));
        set({ waitForRender: true });
      },
      setWaitForRender: (state) =>
        // set((state) => ({ waitForRender: !state.waitForRender })),
        set({ waitForRender: state }),
      setMetadata: (metaData) => set({ metaData }),
    }),
    "Main"
  )
);

// todo: try to group into sets
// and then import sets with useMapStore({Menu})
// which maps all state&actions of Menu group
// menu{...,....,...}
// menu
