import create from "zustand";
import { devtools } from "zustand/middleware";

export const useInputsStore = create(
  devtools(
    (set, get) => ({
      keyboardEvents: {},
      setKeyboardEvents: (keyboardEvents) => set({ keyboardEvents }),

      joystickEvents: {},
      setJoystickEvents: (stateName, events) =>
        set({ [stateName]: { ...events } }),
    }),
    "Inputs"
  )
);
