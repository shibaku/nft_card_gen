import shallow from "zustand/shallow";
import { config } from "./mapStore.config.js";

// ** map store generator
function createMapStore(storeName, props) {
  const storeSelection = {};
  props.forEach((prop) => {
    const state = storeName((state) => state[prop]);
    storeSelection[prop] = state;
    if (state === undefined && config.warOnUndefined)
      console.warn(`warning from useMapStore: state ${prop} is undefined`);
  });
  return storeSelection;
}

function createMapStoreArray(storeName, props) {
  return storeName(
    (state) =>
      props.map((prop) => {
        if (state[prop] === undefined && config.warOnUndefined)
          console.warn(
            `warning from useMapStoreArr: state ${prop} is undefined`
          );
        return state[prop];
      }),
    shallow
  );
}

// **map store functions
export function useMapStore([...props], store = "default") {
  // ! add error catch for stores[store] doesn't exist
  return createMapStore(config.stores[store], props);
}
// !adapt to new syntax
export function useMapStoreArr(...props) {
  return createMapStoreArray(config.stores.default, props);
}
