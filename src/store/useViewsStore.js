import create from "zustand";
import { devtools } from "zustand/middleware";
import produce from "immer";
import { shibakuVerse, gallery } from "views";
import { cloneDeep } from "lodash";

export const useViewsStore = create(
  devtools(
    (set, get) => ({
      updateView: (view, key, stateUpdate) =>
        set(
          produce((state) => {
            state[view][key] = stateUpdate;
          })
        ),
      "shibaku-verse": shibakuVerse.scene,
      gallery: gallery.scene,
    }),
    "Views"
  )
);

const initialViews = cloneDeep(useViewsStore.getState());
export function resetStore() {
  useViewsStore.setState(initialViews);
}
