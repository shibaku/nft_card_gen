import { useStore } from "./useStore";
import { useHaikuStore } from "./useHaikuStore";
import { useAppStore } from "./useAppStore";
import { useViewsStore } from "./useViewsStore";
import { useInputsStore } from "./useInputsStore";

export const config = {
  stores: {
    default: useStore,
    haiku: useHaikuStore,
    app: useAppStore,
    views: useViewsStore,
    inputs: useInputsStore,
  },
  // !fix spelling
  warOnUndefined: true,
};
