import create from "zustand";
import { devtools } from "zustand/middleware";
import produce from "immer";
import { useViewsStore } from "./useViewsStore";
import { set as setLo, get as getLo } from "lodash";
import { cloneDeep } from "lodash";

export const useAppStore = create(
  devtools(
    (set, get) => ({
      // ! maybe one setController can replace all other set functions??
      // ! replace specific imported controller methods with setController in app where possible
      // or do some set functions need to pass special params? like some need active or other setting?
      // can differences be handled by action type??
      // each animation type would set an active state for example, while toggle type would inverse state etc. etc.
      setControllers: (actions, actionPath) =>
        set((state) => {
          actions.forEach((action, index) => {
            if (!action?.disable) {
              // disable single use actions
              if (action.config.useOnce) {
                state.addToViewStoreFifo(action, actionPath, index, {
                  disable: true,
                });
                // state.updateViewStore(action, actionPath, index, {
                //   disabled: true,
                // });
              }
              const stateGr = action.config?.stateGroup || "default";

              // change and update stateGroup?
              if (
                !(stateGr === state.stateGroup) &&
                !action.config.ignoreStateGroup
              ) {
                state.reverseStateGroupCache();
                state.setStateGroup(stateGr);
              }

              // write to stateGroupCache?
              if (
                action?.defaultState &&
                !action.config.useOnce &&
                !action.config.ignoreStateGroup
              ) {
                state.addToStateGroupCache({ action, actionPath, index });
              }

              if (action?.controller === "updateViewsStoreTarget") {
                state.addToViewStoreFifo(action);
              } else {
                // call controller function
                state[action.controller]({
                  //todo: dont destructure state and config cant access the complete state update
                  // like this inside of the setter functions, but want to be able to update complete
                  // state from action
                  ...action.state,
                  ...action.config,
                  onComplete: action.onComplete,
                  path: actionPath + `[${index}]`,
                  // pathIndex: index,
                });
              }
            }
          });
        }),

      stateGroup: "default",
      stateGroupCache: [],
      setStateGroup: (group) => set({ stateGroup: group }),
      addToStateGroupCache: (action) =>
        set(
          produce((state) => {
            state.stateGroupCache.push(action);
          })
        ),
      reverseStateGroupCache: () =>
        set((state) => {
          state.stateGroupCache.forEach((item) => {
            if (item.action?.controller === "updateViewsStoreTarget") {
              // console.log(item);
              // item.action.state = item.action.defaultState;
              const updatedAction = produce(item.action, (draft) => {
                draft.state = item.action.defaultState;
              });
              state.addToViewStoreFifo(
                // item.action
                updatedAction
                // item.actionPath,
                // item.index
              );
            } else {
              // call controller function
              state[item.action.controller]({
                //! dont destructure state and config cant loop over whole state like this
                ...item.action.state,
                ...item.action.config,
                onComplete: item.action.onComplete,
                // !do i need path here as well?
                path: item.actionPath + `[${item.index}]`,
              });
            }
          });
          set({ stateGroupCache: [] });
        }),
      //!extend update to include update: {{....},state:{...},config:{...}}
      // at the moment only root {...} is working
      updateViewStore(action, actionPath, index, update) {
        const updatedView = { ...useViewsStore.getState()[action.config.view] };
        let actions = getLo(updatedView, actionPath);
        const actionsUpdate = produce(actions, (draft) => {
          draft[index] = { ...action, ...update };
        });
        setLo(updatedView, actionPath, actionsUpdate);
        useViewsStore.setState({ [action.config.view]: updatedView });
      },

      // !merge this into above
      updateViewsStoreTarget: (action) => {
        // console.log(action);
        const targetPath = action.config.targetPath;

        const updatedView = {
          ...useViewsStore.getState()[action.config?.view],
        };
        const entity = getLo(updatedView, targetPath);
        const entityUpdate = produce(entity, (draft) => {
          Object.entries(action.state).forEach(([key, value]) => {
            // setLo(updatedView, `${targetPath}.${key}`, value);
            draft[key] = value;
          });
        });
        setLo(updatedView, targetPath, entityUpdate);
        useViewsStore.setState({ [action.config.view]: updatedView });
      },

      viewStoreFifo: [],
      addToViewStoreFifo: (action, actionPath, index, update) =>
        set(
          produce((state) => {
            state.viewStoreFifo.push({ action, actionPath, index, update });
          })
        ),
      shiftViewStoreFifo: () =>
        set(
          produce((state) => {
            state.viewStoreFifo.shift();
          })
        ),

      camera: {},
      setCamera: (props) => set(() => ({ camera: props })),

      cameraTween: {
        active: false,
        position: [0, 0, 0],
      },
      setCameraTween: (props) => {
        // console.log(props);
        set({ cameraTween: { active: true, ...props } });
      },
      cameraTargetTween: {
        active: false,
        target: [0, 0, 0],
      },
      setCameraTargetTween: (props) => {
        set({ cameraTargetTween: { active: true, ...props } });
      },

      modal: { active: false },
      toggleModal: () =>
        set((state) => ({ modal: { active: !state.modal.active } })),
      // set(
      //     produce((state) => {
      //       state.modal.active = !state.modal.active;
      //     })
      //   ),
      updateModal: (props) => set({ modal: props }),
      // set(
      //   produce((state) => {
      //     state.modal = { ...props };
      //   })
      // ),

      gltfAnimations: {},
      addModelNameToAnim: (modelName) => {
        set(
          produce((state) => {
            if (modelName in state.gltfAnimations) return;
            state.gltfAnimations[modelName] = {};
          })
        );
      },
      setGltfAnimation: ({ modelName, animation, ...props }) =>
        set(
          produce((state) => {
            state.gltfAnimations[modelName][animation] = {
              ...props,
              active: true,
              // ! find a better way to do below, maybe a combined setGltfAnim and updateGltf
              // ! maybe mix booth into one function? that more flexible
              running: state.gltfAnimations[modelName][animation]?.running,
            };
          })
        ),
      updateGltfAnimation: (animationName, updateObj) => {
        set(
          produce((state) => {
            Object.keys(state.gltfAnimations).forEach((model) => {
              if (animationName in state.gltfAnimations[model]) {
                Object.entries(updateObj).forEach(([key, value]) => {
                  state.gltfAnimations[model][animationName][key] = value;
                });
              }
            });
          })
        );
      },
      audio: {},
      setAudio: (action) =>
        set(
          produce((state) => {
            // if active: false as extra prop also remove
            if (action.name in state.audio) {
              delete state.audio[action.name];
              return;
            }
            state.audio[action.name] = { ...action };
          })
        ),
      fpsController: {
        forward: false,
        backward: false,
        left: false,
        right: false,
        jump: false,
        rotateLeft: false,
        rotateRight: false,
        rotateUp: false,
        rotateDown: false,
      },
      setFpsController: (action) => {
        set(
          produce((state) => {
            // todo: once refactor is done for keeping state as object in setController
            // update directly the direction and use rotateX: -1 or 1 for direction
            state.fpsController[action.direction] = action.active;
          })
        );
      },
      transFormController: {
        active: false,
        name: null,
        mode: "translate",
      },
      setTransFormController: (newState) =>
        set((state) => {
          const stateUpdate = { ...state.transFormController, ...newState };
          // console.log(stateUpdate);
          set({
            transFormController: stateUpdate,
          });
        }),
    }),
    "AppState"
  )
);

const initialApp = cloneDeep(useViewsStore.getState());
export function resetStore() {
  useViewsStore.setState(initialApp);
}
