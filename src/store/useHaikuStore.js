import create from "zustand";
import { devtools } from "zustand/middleware";

export const useHaikuStore = create(
  devtools(
    (set, get) => ({
      haikuArr: [],
      setHaikuArr: (input) => set({ haikuArr: input }),
    }),
    "Haiku"
  )
);
