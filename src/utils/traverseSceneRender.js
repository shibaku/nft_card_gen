import defaults from "data/sceneDefaults.csv";
import metaLayerMatch from "./metaLayerMatch";

const sceneDef = csvArrToObj(defaults);

export default function traverseScene(
  parentNode,
  children,
  childrenVis = false,
  metaData
) {
  if (children.length === 0) return;

  // loop through all children of current graph level
  const childrenNodes = children.map((node, index) => {
    let nodeChildrenVis = false;
    node.visible = false;

    // !restructure some of this to make easier to comprehend
    const match = metaLayerMatch(node, metaData);
    // node or parent in scene default setup def && handle is visibleChild
    if (node.name in sceneDef || childrenVis || node.parent.name in sceneDef) {
      // mode: nonToggle ||childrensvis => node, subnodes on
      if (sceneDef[node.name] === "nonToggle" || childrenVis) {
        node.visible = true;
        nodeChildrenVis = true;
        // mode: default toggle ||on =>only node
      } else if (
        sceneDef[node.name] === "toggle" ||
        sceneDef[node.parent.name] === "toggle" ||
        sceneDef[node.name] === "on"
      ) {
        node.visible = true;
        // mode: match metadata => node, subnodes on
      } else if (match) {
        node.visible = true;
        nodeChildrenVis = true;
      }
      // not in scene default setup, only metadata match
    } else if (match) {
      node.visible = true;
      nodeChildrenVis = true;
    }

    const graphIndex = [...parentNode.graphIndex, index];
    const currentNode = {
      name: node.name,
      type: node.type,
      parent: node.parent.name,
      visible: node.visible,
      graphIndex,
    };
    if (node.children.length > 0)
      traverseScene(currentNode, node.children, nodeChildrenVis, metaData);
    return currentNode;
  });
  // attach the whole scene node to the initial parent
  parentNode.children = childrenNodes;
  return parentNode;
}

function csvArrToObj(csv) {
  csv.shift();
  const csvObj = {};
  // console.log(csv);
  csv.forEach((row) => {
    csvObj[row[0]] = row[1];
  });
  return csvObj;
}
