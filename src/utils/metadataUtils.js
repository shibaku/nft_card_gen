export function mapCsvToObj(mapCSV) {
  // keys are first line of csv
  // const keys = mapCSV.shift().map((entry) => entry);
  const layerMatch = {};
  mapCSV.forEach((attribute) => {
    const key = attribute[0].replaceAll(" ", "_").toLowerCase();
    layerMatch[key] = {
      value: attribute[1].toLowerCase(),
      shift: attribute[2],
      pop: attribute[3],
    };
  });
  return layerMatch;
}

// todo: rename this
export function metaDataCsvToObj(data, mapMetaToStore) {
  console.log(mapMetaToStore);
  const metaData = {};
  data.attributes.map((attribute) => {
    const traitType = attribute.trait_type;
    const key = traitType?.replaceAll(" ", "_")?.toLowerCase();
    if (mapMetaToStore[traitType]) {
      const value = mapMetaToStore[traitType][attribute.value];
      return (metaData[key] = value);
    } else {
      const value =
        typeof attribute.value === "string"
          ? attribute.value.replaceAll(" ", "_").toLowerCase()
          : attribute.value;
      return (metaData[key] = value);
    }
  });
  return metaData;
}

export function exceptionCsvToObj(exceptionCsv) {
  const exceptions = {};
  const exceptCopy = [...exceptionCsv];
  exceptCopy.shift();
  exceptCopy.map(
    (exception) =>
      (exceptions[exception[0]] = {
        enabled: exception[1].replaceAll(" ", "_").toLowerCase().split(","),
        disabled: exception[3].replaceAll(" ", "_").toLowerCase(),
      })
  );
  return exceptions;
}

export function tokenIdDigits(id) {
  const digits = id.toString().split("");
  let reversedDigits = digits.map(Number).reverse();
  const lengthDiff = 6 - reversedDigits.length;
  if (lengthDiff > 0) {
    const filler = new Array(lengthDiff).fill(0);
    reversedDigits = reversedDigits.concat(filler);
  }
  return reversedDigits;
}

export function getHaikuNodes(mapHaikuCsv, haiku) {
  const haikuArr = haiku.replaceAll("_", " ").split("\n");
  const haikuNodes = [];
  mapHaikuCsv.forEach((line) => {
    const haikuLineSan = line[0].replaceAll(",", "").replaceAll('"', "");
    // const haikuLineSan = line[0]; //! no string clean up
    if (haikuArr.includes(haikuLineSan)) {
      haikuNodes.push(line[1].toLowerCase());
    }
  });
  return haikuNodes;
}

export function mapMetaToStoreCsvToObj(csv) {
  const obj = {};
  csv.forEach((row) => {
    const type = row[0];
    const value = row[1];
    const mapped = row[2];
    if (!obj[type]) obj[type] = {};
    obj[type][value] = mapped;
  });
  return obj;
}
