export default function metaLayerMatch(node, metaData) {
  if (!metaData) return;
  const parentN = node.parent.name.toLowerCase();
  let nodeN = node.name.toLowerCase();

  if (!(parentN in metaData.mapMeta)) return false;
  const mapCurrent = metaData.mapMeta[parentN];

  // if name remap get new value
  if (mapCurrent.value)
    nodeN = mapCurrent.value.replaceAll(" ", "_").toLowerCase();
  // remove characters in front
  if (mapCurrent.shift) nodeN = nodeN.substr(mapCurrent.shift);
  // remove characters in back
  if (mapCurrent.pop) nodeN = nodeN.slice(0, -mapCurrent.pop);

  // check for exception - isGolden?
  const ex = isException(
    metaData.isGolden,
    metaData.exceptions.Golden,
    nodeN,
    parentN,
    metaData.metaData
  );
  if (ex === "on") return true;
  if (ex === "off") return false;
  // check if node is haiku
  if (isHaiku(nodeN, metaData.haikuNodes)) return true;
  // handle Token ID
  if (isTokenId(metaData.tokenIdArr, nodeN, parentN)) return true;
  // check if node is in Metadata
  if (metaData.metaData[parentN] === nodeN) return true;
  // no match
  return false;
}

function isException(isException, exception, nodeN, parentN, metaData) {
  if (!isException) {
    if (nodeN === exception?.disabled || parentN === exception?.disabled) {
      return "on";
    }
  } else {
    if (
      exception?.enabled.includes(nodeN) ||
      exception?.enabled.includes(parentN)
    ) {
      return "on";
    }
    if (metaData.cartridge === nodeN || metaData.costume === nodeN) {
      return "off";
    }
  }
  return "pass";
}

function isTokenId(tokenIdArr, nodeN, parentN) {
  const parentNumb = Number(parentN);
  if (!tokenIdArr[parentN] && !(tokenIdArr[parentN] === 0)) return;
  const nodeNumb = Number(nodeN.split("")[1]);
  if (tokenIdArr[parentNumb] === nodeNumb) return true;
  return false;
}

function isHaiku(nodeN, haikuNodes) {
  if (haikuNodes.includes(nodeN)) return true;
  return false;
}
