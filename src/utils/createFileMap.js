export default function createFileMap(acceptedFiles) {
  let rootFile;
  let rootPath;

  // find root file
  for (let file of acceptedFiles) {
    if (file.path.match(/\.(gltf|glb)$/)) {
      rootFile = "./" + file.name;
      rootPath = file.path.replace(file.name, "");
      break;
    }
  }

  if (!rootFile) {
    // !!set error message and add to return object
  }

  // turn absolute paths of all files into path relative to root file
  // use relative path as key for fileMap
  const map = Object.fromEntries(
    acceptedFiles.map((file) => [file.path.replace(rootPath, "./"), file])
  );

  // sanitize and get name
  const name = rootFile
    .trim()
    .replace(/\.*\/*/, "")
    .replace(/\.[^/.]+$/, "");

  return { name, rootFile, map };
}
