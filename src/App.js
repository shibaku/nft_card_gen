import { Route, Switch } from "react-router-dom";
import { allRoutes } from "routes";

import {
  computeBoundsTree,
  disposeBoundsTree,
  acceleratedRaycast,
} from "three-mesh-bvh";
import { BufferGeometry, Mesh } from "three";

// add mesh-bvh
BufferGeometry.prototype.computeBoundsTree = computeBoundsTree;
BufferGeometry.prototype.disposeBoundsTree = disposeBoundsTree;
Mesh.prototype.raycast = acceleratedRaycast;

const list = allRoutes?.map(({ imgPath, name, ...props }) => (
  <Route {...props} key={props.path} />
));

export default function App() {
  return (
    <>
      <Switch>{list}</Switch>
    </>
  );
}
