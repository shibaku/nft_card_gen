const path = require("path");
const csvLoader = require("craco-csv-loader");

// craco.config.js
module.exports = {
  webpack: {
    alias: {
      "~": path.resolve(__dirname, "src/"),
    },
  },
  plugins: [
    {
      plugin: csvLoader,
      options: {
        dynamicTyping: true,
        header: true,
        skipEmptyLines: true,
      },
    },
  ],
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
  // if you run test with jest
  // jest: {
  //   configure: {
  //     moduleNameMapper: {
  //       '^~(.*)$': '<rootDir>/src$1'
  //     }
  //   }
  // },
};
