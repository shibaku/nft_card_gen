const colors = require("tailwindcss/colors");

module.exports = {
  mode: "jit",
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      boxShadow: {
        white: "0px 0px 7px 4px rgba(255, 255, 255, 1)",
      },
    },
    colors,
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
