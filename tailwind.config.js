// const colors = require("tailwindcss/colors");

module.exports = {
  mode: "jit",
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      boxShadow: {
        white: "0px 0px 7px 4px rgba(255, 255, 255, 1)",
      },
    },
    // colors,
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
